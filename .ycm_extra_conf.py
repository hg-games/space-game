def Settings( **kwargs ):
  return {
    'flags': [ '-x', 'c++', '-Wall', '-Wextra', '-Wno-missing-braces',
               '-std=c++17',
               '-Isource/src/',
               '-Isource/subprojects/fmt-5.3.0/include/',
               '-Isource/subprojects/SFML-2.5.1/include/',
               '-Isource/subprojects/entt-3.0.0/src/',
               '-Isource/subprojects/json-3.6.1/include/',
               '-Isource/subprojects/spdlog-1.7.0/include/',
               '-Isource/subprojects/stb_rect_pack/',
               '-Isource/subprojects/lua-5.3.5/src/',
               '-Isource/subprojects/libnoise-1.0.0/noise/include/',
               '-Isource/subprojects/libnoise-1.0.0/', # for noiseutils
               '-Isource/subprojects/imgui-sfml/',
               '-Isource/subprojects/box2d/include/',
               '-Isource/subprojects/glm-0.9.9.8/',
             ],
  }
