# Every new idea goes there

They should be sorted by order of importance and difficulty to implement.

 - power generation with solar panels
 - power generation with nuclear reactor
 - need for oxygen/atmosphere to support organic life
 - creation of elements by using some kind of fusion with lasers all around used to compress material at the center of focus

## Gameplay Ideas
 
 - Player needs to build a space station for people that are currently in stasis
 - Start in the asteroid belt?
 - Need to exploit resources from the belt to build things from the ground up?
 - Maybe add some enemies and weapons?
