# Documentation for the physics of the whole simulation

## Spaceship

A spaceship is a assembly of b2Body where parts are composed of one or more fixtures and each the bodies are joined together with joints (so that they can break on collision/overload)

Box2d force/impulse are used to simulate engine thrust and have an effect on the spacecraft. Box2d is also used to simulate collisions between the spaceship and the planetary body when the surface is a solid and when the speeds are low enough.

Above a velocity threshold, the spacecraft will simply disintegrate on impact (or maybe have a few random bits of some sturdier parts survive?). So Box2d is not used for high speed collisions.

## High velocity

Since Box2d enforces a limit on the velocity (depends on the step time, by default it's 2x UPS), the velocity of the spacecraft will not be accurate by relying only on the physics simulation. So what we need to do is step the Box2d simulation, get all the thrust vectors from every activated engines, get all the external forces (gravity, drag) and compute the velocity vector for this frame, apply it to the current velocity.

That way we can keep the ship's simulation and have high velocity to achieve orbits. External forces are also applied to the Box2d simulation so that parts can brake if the drag is too much or the gravity causes the ship to fall down the launch pad.

## Planet Gravity

Gravity is applied to spaceships that are in atmosphere. When the spaceship leaves the atmosphere, it enters the "in orbit" state and we rely on the orbital trajectory.

## Orbital mechanics

Another simulation will be used for the orbital simulation. Ships will be simplified to a single point so that the simulation can be fast-forwarded.

## Constant thrust

Maybe add some kind of constant thrust ability to the orbital simulation so that we can have "short" transfer time between bodies. This implies that the orbital simulation is aware of ship thrusters so that it can apply the thrust even when fast-forwarding.
