# Documentation for the different game states and views

## States

States can only be executed one at a time.

### MainMenu

First state loaded by the game, used to load a save, change settings or quit the game.

### VesselEditor

State where the player will be able to load/edit/save vessels.

### Game

Main state. In this state the time since epoch ticks, planets and vessels travel along their orbits and vessels that are not on rails get simulated.

## Views

Views are used to get a different representation for a given state. Views can be stacked on top of one another.

So when the player enters the planetarium the planetarium view is created. Then he selects a vessel and load it, the VesselView replaces the planetarium.

But when the player is in the VesselView and switch to the planetarium, we keep the VesselView loaded because it takes longer to rebuild the VesselView (need to recreate the simulation world).

### Planetarium

View for the solar system and the vessels travelling in space. Load a basic representation of the planets (colored sphere) and vessels (icon).

The player will be able to select planets/vessels and view basic informations (velocity, mass, ...).

From this view the player will be able to enter VesselView for any flying vessels.

### VesselView

In this view the player will see the focused vessel and any vessel in close proximity. From this view the player will also be able to control the vessel (thrusters, actuators, docking...).

In this view, all loaded vessels will be fully simulated until the view is destroyed.

The player is only able to leave this view if all the loaded vessels can enter the "on rails" state.
