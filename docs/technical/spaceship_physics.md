# Documentation on the physics of different parts of a spaceship

## Thruster

Specification of the thrust of an engine

  - vCOM:   vector to center of mass
  - vT:     thrust vector
  - thrust: engine thrust (in a vacuum)

## Engine

Engine specification

  - mass
  - fuel consumption
  - Isp (in a vacuum for now)
  - gimbal (later)

## Thrust balancing

Goal: given a spaceship with a set of thruster, balance the thrust of each individual engine as best as possible to move in the intended direction.

How: each engine is defined by its vCOM, vT and thrust (max thrust). Each engine will apply a torque at the center of mass. We need to solve an equation to find a set of throttle values so that applied_torque ~= target_torque.

Steps:

 - find if we need a change in velocity (user input, controller). 
 - with a simple vector projection of each vT to the target velocity vector, figure out each initial throttle
 - solve each engine throttle to satisfy target torque
 
$`\tau_T`$ is the target torque\
$`C_i`$ is the constant for the engine $`i`$ defined by $`C_i = \overrightarrow{CoM_i}\times\overrightarrow{T_i}*thrust_i`$\
The torque applied by each engine is given by $`\tau_i = C_i*t_i`$ where $`t_i`$ is the throttle value for the engine between 0 and 1.\
We need to satisfy $`\tau_T = \sum_{i=1}^{n}\tau_i`$, so we can rewrite each engine equation this way:

```math
k \in [1, n], \tau_k = -(\sum_{i=1}^{k-1}C_{i}t_i + \sum_{i=k+1}^{n}{C_{i}t_i} - \tau_T)
```
which can be rewritten as:

```math
k \in [1, n], t_k = -(\sum_{\mathclap{i\in[1,n]\setminus{k}}}{C_{i}t_i}\quad- \tau_T)/C_k
```
