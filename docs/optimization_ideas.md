## TileMap

### Renderer

Render visible chunk (with 1 or 2 more on each sides) to a render texture and when rendering the tilemap, check if the chunk rendered into the texture from a previous frame are still valid (the chunks we need to draw this frame are all already rendered, and the rendered chunks have not been modified)

If the render texture is still valid, all that is needed to draw the tilemap is render a fullscreen quad with the correct texture coords.

(That's how factorio does it (I think))
