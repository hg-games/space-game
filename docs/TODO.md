- General
	- move prototype loading code for astrobodies out of the Planetarium class
	- figure out a way to have different views for the same scene

- DataLoader
	- make a global class that handles loading

- Prototype Repository
	- add some debug infos about loaded prototypes (inspection maybe?)

- Orbit
	- rework the whole class to use Universal Variable Formulation (https://github.com/rikusalminen/twobody)
	- properly handle all types of orbits

- Scenes
	- Game
		- initialize solar system
		- initialize default vessel
		- VesselView 
			- add player input control
			- load close vessels
			- switch between visible vessels
			- interaction with vessel parts
		- MapView
			- add auto maneuvers (creation/execution)
			- switch between bodies/vessels
			- view comlinks
			- selected object infos (speed, alt, mass)
			- advanced infos for bodies (atmo comp, soil comp, ...)
	- VesselEditor
		- add vessel preview
		- add GUI for parts
		- construction system
		- parts editing (size, fuel type, ...)
		- display infos (TWR, dV, mass, ...)
		- save/load
		- action groups
		- scripted actions
	- Loading
		- make simple loading screen
		- add text/updatable text
	- MainMenu
		- add settings
		- add save/load game
		- add start new

- Assets
	- Graphics
		- create some basic textures for testing some parts (structural/fuel tank/engine)
		- figure out how terrain is going to be drawn
		- UI style
	- AstroBodies
		- create terrain data for earth (just test data for now)
		- maybe have a random generator for the asteroids terrain and just store a seed number?

