# Story ideas

See individual files for more details.

## Story

Governing body of Earth is doing something questionable and needs to be stopped (or not, maybe leave the choice to the player)

Depending on player actions, he'll be hunted or left alone

## Story - intro

The player "wakes up" inside a room and discover what he is gradually. It's unknown why he woke up, he was not supposed to.
First goal is to get out, explore and learn how to play the game.

## Story - goal ideas
 - Earth was destroyed. Discover why and fix it (or not)
 - Earth became a dystopia, figure out who's in charge and overthrow them or work with them
 - Start on an unknown planet (the player figure it out later). Need to survive, go back to earth and fix stuff (maybe chain this goal with another one from earth)
 - Start on Mars, discover a conflict between Earth and Mars (kinda like the expanse)
 - Player must fight a rogue AI (so the AI would attack the player's base in waves through portals)
