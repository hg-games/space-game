# General ideas for gameplay

See other files in this directory for more details on some concepts.

## General

 - Main opponent is a rogue AI
 - Player has a main base of operation that he can develop over time
 - Player starts from a platform that can deploy miner drones, recon drone and basic material processing

## Resources Collection

 - need to setup mining outposts to gather and process raw resources to send them to a processing facility and create new vessels
 - automate science experiments
 - automated vessel travel to deliver resource around facilities
 - mining facilities on the planets and on asteroids
 - gas giants exploitation
 - sun energy harvesting
 - advanced production and refinement process for late game (antimatter, FTL fuel?)

## Tech progression

 - Discover and unlock technologies (using research point?)
 - Maybe add specific equipment to do the research
 - Unlock new abilities, parts, ...

