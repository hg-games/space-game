#pragma once

#include "spdlog/sinks/ringbuffer_sink.h"
#include "spdlog/spdlog.h"

extern const std::shared_ptr<spdlog::logger>              g_logger;
extern std::shared_ptr<spdlog::sinks::ringbuffer_sink_mt> g_logger_ringbuffer;
