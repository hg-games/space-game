#include "ResourceHandler.hpp"

#include "SFML/Graphics/Font.hpp"
#include "SFML/Graphics/Texture.hpp"

#include "TextureAtlas.hpp"

ResourceHandler* g_resources = nullptr;

void ResourceHandler::create() {
	if(!g_resources) g_resources = new ResourceHandler;
}

ResourceHandler::ResourceHandler() {
	m_defaultFont = new sf::Font;
}

void ResourceHandler::loadDefaultFont(std::string_view path) {
	m_defaultFont->loadFromFile(path.data());
}

void ResourceHandler::loadTexture(size_t id, std::string_view path) {
	if(m_textures.count(id) == 0) {
		auto t = new sf::Texture;
		t->loadFromFile(path.data());
		m_textures[id] = t;
	}
}

sf::Texture* ResourceHandler::getTexture(size_t id) {
	if(m_textures.count(id) > 0) return m_textures[id];
	return nullptr;
}

TextureAtlas* ResourceHandler::createTextureAtlas(size_t id) {
	if(m_atlases.count(id) == 0) m_atlases[id] = new TextureAtlas(2048, 2048);
	return m_atlases[id];
}

TextureAtlas* ResourceHandler::getTextureAtlas(size_t id) {
	if(m_atlases.count(id) > 0) return m_atlases[id];
	return nullptr;
}
