#include "Game.hpp"

#include "PrototypeRepo.hpp"
#include "Prototypes/PlanetarySystemPrototype.hpp"

namespace Scene {
Game::Game(SceneStack& stack, Context context):
    Base(stack, context), m_view(m_planetarium), m_objView(m_planetarium) {
	m_planetarium.loadFromPrototype(*std::static_pointer_cast<PlanetarySystemPrototype>(
	    g_prototypeRepo->getPrototypeByName("solar-system"_hs)));

	m_view.init();
	m_objView.init();

	m_currentView = &m_objView;
}

bool Game::render() const {
	m_currentView->render(*getContext().target);
	return false;
}

bool Game::step() {
	m_planetarium.step();
	m_currentView->step();
	return false;
}

bool Game::handleIOEvent(sf::Event const& event) {
	m_currentView->handleIOEvent(event);
	return false;
}

void Game::handleWindowResize(sf::Vector2i const& size) {
	m_currentView->handleWindowResize(size);
}

} // namespace Scene
