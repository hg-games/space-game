#pragma once

#include <optional>

#include "Base.hpp"

#include "Camera.hpp"
#include "FSM.hpp"
#include "QuadTree.hpp"

#include "Entities/Vessel.hpp"
#include "Parts/Base.hpp"
#include "Prototypes/PartPrototype.hpp"

namespace Scene {
class Editor final: public Base {
public:
	Editor(SceneStack& stack, Context context);

	bool render() const override;
	bool step() override;
	bool handleIOEvent(sf::Event const& event) override;
	void handleWindowResize(sf::Vector2i const& size) override;

private:
	void setupFSM();

	void renderVessel() const;
	void renderPart(sf::RenderTarget& target, std::shared_ptr<Part::Base> const& part) const;
	void renderHighlightPart(sf::RenderTarget&                  target,
	                         std::shared_ptr<Part::Base> const& part) const;
	void UI();

	void newEmptyVessel();
	bool placePartInHand();
	void snapPartToVessel(std::shared_ptr<Part::Base> part, sf::Vector2f const& mouse_pos);
	void linkMountPoints(MountPoint* part_mp, MountPoint* vessel_mp);
	void unlinkMountPoint(MountPoint* mp);
	void clearHand();

	std::shared_ptr<Part::Base> createPartFromPrototype(std::uint32_t name_hash) const;

private:
	Camera m_camera;

	FSM m_fsm;

	std::shared_ptr<Vessel>     m_vessel;
	std::shared_ptr<Part::Base> m_handPart;
	std::shared_ptr<Part::Base> m_selected;

	// editing related vars
	float        m_origAngle;
	bool         m_rotateStep {false};
	sf::Vector2f m_dragStart;
	bool         m_dragNoCheck {false};
	bool         m_dragIsLinked {false};

	struct SnapContext {
		MountPoint* hand_mp;
		MountPoint* target_mp;
	};
	std::optional<SnapContext> m_snapContext;

	using QuadTreeFloatType = float;
	using QElem             = std::shared_ptr<Part::Base>;
	Quadtree<QElem, sf::Rect<QuadTreeFloatType>(QElem const&), QuadTreeFloatType> m_partsQuadtree;
};
} // namespace Scene
