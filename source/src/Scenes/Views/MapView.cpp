#include "MapView.hpp"

#include <cmath>

#include "SFML/Graphics/CircleShape.hpp"
#include "SFML/Graphics/RenderTarget.hpp"
#include "SFML/Window/Event.hpp"

#include "Logger.hpp"
#include "Planetarium.hpp"
#include "Renderer/DebugRenderer.hpp"
#include "constants.hpp"
#include "utils.hpp"

#include "imgui.h"

MapView::MapView(Planetarium& planetarium):
    m_planetarium(planetarium), m_orbitVertices(sf::LineStrip, sf::VertexBuffer::Usage::Static) {
	m_camera.setCenter(0, 0);
	g_debugRenderer->set_camera(&m_camera);
}

void MapView::init() {
	std::vector<sf::Vertex> orbits;
	// the planets and moons have a static orbit so upload them to the GPU
	for(auto const& bptr: m_planetarium.getBodies()) {
		const auto& orbit = bptr->orbit;
		if(!orbit) continue;
		auto& [start, size] = m_indexBuffer[bptr->name_hash];
		auto const& vec     = getOrbitVertices(*orbit);

		start = orbits.size();
		std::copy(vec.begin(), vec.end(), std::back_inserter(orbits));
		size = orbits.size() - start;
	}
	m_orbitVertices.create(orbits.size());
	m_orbitVertices.update(orbits.data());
}

void MapView::step() {
	ImGui::SetNextWindowPos({0.f, 100.f});
	ImGui::Begin("Map View");
	ImGui::Text("Zoom: %.4f", m_camera.getZoom());
	ImGui::Separator();
	if(ImGui::Button("-")) {
		m_planetarium.setWarpSpeed(m_planetarium.getWarpSpeed() / 10);
	}
	ImGui::SameLine();
	if(ImGui::Button("+")) {
		m_planetarium.setWarpSpeed(m_planetarium.getWarpSpeed() * 10);
	}
	ImGui::SameLine();
	ImGui::Text("Warp: %d", m_planetarium.getWarpSpeed());
	if(ImGui::Button("Pause")) {
		m_planetarium.togglePause();
	}

	if(const auto ptr = m_focused_entity.lock(); ptr) {
		switch(ptr->type) {
			// case Entity::Type::Sun:
			case Entity::Type::Planet: {
				const auto bptr = std::static_pointer_cast<Astrobody>(ptr);
				if(bptr->orbit) {
					auto const& pos = m_planetarium.getBodyPosition(bptr->name_hash);
					m_camera.setCenter(pos.x * renderScale, pos.y * renderScale);
					m_cameraNoZoom.setCenter(pos.x * renderScale, pos.y * renderScale);
					ImGui::Separator();
					ImGui::Text("Focused Body: %s", bptr->name.data());
					ImGui::Text("Position: %.4f,%.4f", pos.x * renderScale, pos.y * renderScale);
				}
			} break;
			default: break;
		}
	}
	ImGui::End();
}

void MapView::render(sf::RenderTarget& target) const {
	target.setView(m_camera);
	for(auto const& [hash, pair]: m_indexBuffer) {
		auto const& [start, size] = pair;
		sf::RenderStates states;
		const auto       shift = m_planetarium.getParentBodyPosition(hash) * renderScale;
		states.transform.translate(shift.x, shift.y);
		target.draw(m_orbitVertices, start, size, states);
	}

	target.setView(m_cameraNoZoom);

	renderBody(target, m_planetarium.getRoot());
	for(auto const& bptr: m_planetarium.getBodies()) {
		renderBody(target, bptr);
	}
}

void MapView::handleIOEvent(sf::Event const& event) {
	switch(event.type) {
		case sf::Event::MouseWheelScrolled:
			if(event.mouseWheelScroll.delta > 0)
				m_camera.zoom(0.9);
			else if(event.mouseWheelScroll.delta < 0)
				m_camera.zoom(1.1);
			break;
		case sf::Event::KeyPressed:
			if(event.key.code == sf::Keyboard::Tab) {
				if(m_focused_entity.expired()) {
					focusEntity(m_planetarium.getRoot());
				}
				else if(m_focused_entity.lock()->name_hash == m_planetarium.getRoot()->name_hash) {
					focusEntity(m_planetarium.getBodies().front());
				}
				else {
					auto const& bodies = m_planetarium.getBodies();
					auto        it = std::find_if(bodies.begin(), bodies.end(), [this](auto&& ptr) {
                        return ptr->name_hash == m_focused_entity.lock()->name_hash;
                    });
					if(it != bodies.end()) {
						++it;
						if(it == bodies.end()) it = bodies.begin();
						focusEntity(*it);
					}
				}
			}
			break;
		default: break;
	}
}

void MapView::handleWindowResize(sf::Vector2i const& size) {
	m_camera.setSize(size.x, size.y);
	m_cameraNoZoom.setSize(size.x, size.y);
}

void MapView::focusEntity(std::shared_ptr<Entity> entity) {
	if(!entity) {
		m_focused_entity.reset();
		return;
	}

	g_logger->info("focusing on [{}]", entity->name);
	m_focused_entity = entity;
}

std::vector<sf::Vertex> MapView::getOrbitVertices(Orbit const& orbit) const {
	std::vector<sf::Vertex> ret;

	const auto ca = std::cos(orbit.arg_of_periapsis * utils::degToRad);
	const auto sa = std::sin(orbit.arg_of_periapsis * utils::degToRad);

	const auto semi_major_axis = orbit.semi_major_axis * renderScale;
	const auto semi_minor_axis = orbit.semi_major_axis
	                             * std::sqrt(1 - orbit.eccentricity * orbit.eccentricity)
	                             * renderScale;
	const auto c      = orbit.semi_major_axis * orbit.eccentricity * renderScale;
	const auto center = -sf::Vector2<double>(std::cos(orbit.arg_of_periapsis * utils::degToRad),
	                                         std::sin(orbit.arg_of_periapsis * utils::degToRad))
	                    * c;

	constexpr int min = 128;
	const int     count =
	    std::max(min, static_cast<int>(min * orbit.semi_major_axis / (physics::AU / 10.0)));
	for(int i = 0; i < count; i++) {
		auto&       p = ret.emplace_back();
		const float t = i * 2 * pi / count - pi / 2;

		const float x = semi_major_axis * std::cos(t);
		const float y = semi_minor_axis * std::sin(t);

		p.position.x = x * ca - y * sa + center.x;
		p.position.y = x * sa + y * ca + center.y;
		p.color      = sf::Color::White;
	}
	ret.emplace_back(ret.front());

	return ret;
}

void MapView::renderBody(sf::RenderTarget& target, std::shared_ptr<Astrobody> const& bptr) const {
	sf::CircleShape c;
	auto pos = sf::Vector2f(m_planetarium.getBodyPosition(bptr->name_hash) * renderScale);
	pos      = sf::Vector2f(target.mapCoordsToPixel(pos, m_camera));
	pos      = target.mapPixelToCoords(sf::Vector2i(pos), m_cameraNoZoom);
	c.setPosition(pos);
	c.setRadius(10);
	c.setOrigin(10, 10);
	c.setFillColor(bptr->color);
	target.draw(c);
}
