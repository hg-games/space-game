#include "ObjectView.hpp"

#include "SFML/Graphics/CircleShape.hpp"
#include "SFML/Graphics/RenderTarget.hpp"
#include "SFML/Window/Event.hpp"

#include "box2d/b2_body.h"

#include "DebugInfo.hpp"
#include "Entities/Astrobody/Asteroid.hpp"
#include "Entities/Astrobody/Planet.hpp"
#include "Entities/Vessel.hpp"
#include "Entities/details/Terrain.hpp"
#include "Planetarium.hpp"
#include "PrototypeRepo.hpp"
#include "Prototypes/Astrobody/PlanetPrototype.hpp"

#include "imgui.h"

ObjectView::ObjectView(Planetarium& planetarium): m_planetarium(planetarium) {}

void ObjectView::init() {
	m_terrainRenderer.setCamera(m_camera);

	const auto name_hash = "phobos"_hs;
	const auto body      = m_planetarium.getBody(name_hash);
	const auto ptr       = std::static_pointer_cast<Planet>(body);

	Terrain::GeneratorParams gp;
	gp.base_radius = body->radius;

	ptr->terrain.generate(gp);

	const auto vessel = std::make_shared<Vessel>();
	Orbit      orbit {};
	orbit.eccentricity     = 0.0 * utils::degToRad;
	orbit.arg_of_periapsis = 0.0 * utils::degToRad;
	orbit.mean_anomaly     = 0;
	orbit.semi_major_axis =
	    g_prototypeRepo->getPrototypeByName<PlanetPrototype>(name_hash)->radius + 0'500;
	orbit.setOrbitedBody(name_hash);
	vessel->setOrbit(orbit);
	m_planetarium.insertVessel(vessel);
	vessel->buildBodyFromParts();
	m_focused_entity = vessel;
}

void ObjectView::step() {
	ImGui::Begin("View");

	if(ImGui::Button("-")) {
		m_planetarium.setWarpSpeed(m_planetarium.getWarpSpeed() / 10);
	}
	ImGui::SameLine();
	if(ImGui::Button("+")) {
		m_planetarium.setWarpSpeed(m_planetarium.getWarpSpeed() * 10);
	}
	ImGui::SameLine();
	ImGui::Text("Warp: %d", m_planetarium.getWarpSpeed());
	if(ImGui::Button("Pause")) {
		m_planetarium.togglePause();
	}

	ImGui::Separator();

	if(const auto ptr = m_focused_entity.lock(); ptr && ptr->type == Entity::Type::Vessel) {
		ImGui::Text("Focused on vessel");

		const auto vessel   = std::static_pointer_cast<Vessel>(ptr);
		const auto pos      = m_planetarium.getVesselPosition(vessel->id);
		const auto [fx, fy] = pos;
		const auto vel      = m_planetarium.getVesselVelocity(vessel->id);
		const auto [x, y]   = Terrain::getChunkCoordsFromPosition({fx, fy});
		const auto body =
		    std::static_pointer_cast<Planet>(m_planetarium.getOrbitedBody(vessel->id));

		const sf::Vector2f camera_pos((fx - x * Chunk::size) * meterToPixel,
		                              (fy - y * Chunk::size) * meterToPixel);

		ImGui::Text("Orbited body: %s", body->name.data());
		ImGui::Text("Position: %.1fkm,%.1fkm", fx / 1000.0, fy / 1000.0);
		ImGui::Text("Altitude: %.2fkm", (utils::length(pos) - body->radius) / 1000.0);
		ImGui::Text("Velocity: %.2fkm/s", utils::length(vel) / 1000.0);
		ImGui::Text("Period: %.2fs", vessel->getOrbit().period());
		if(vessel->hasBody()) {
			auto const [bx, by] = vessel->getBody()->GetPosition();
			ImGui::Text("Body Position: %.1fm,%.1fm", bx, by);
		}

		m_terrainRenderer.setTerrain(body->terrain);
		m_terrainRenderer.setChunkCoord(x, y);
		m_camera.setCenter(camera_pos);
		m_renderTerrain = m_terrainRenderer.hasVisibleChunks();

		ImGui::Separator();
		ImGui::Text("Has Chunk? %s", body->terrain.hasChunk(x, y) ? "yes" : "no");
		ImGui::Text("Has Visible Chunks? %s", m_terrainRenderer.hasVisibleChunks() ? "yes" : "no");
		ImGui::Text("Rendered Chunks: %d", g_debugInfo->rendered_chunks);
	}
	else {
		m_renderTerrain = false;
	}

	ImGui::Separator();
	ImGui::Text("Zoom: %.1f", m_camera.getZoom());
	ImGui::End();

	if(const auto ptr = m_focused_entity.lock(); ptr && ptr->type == Entity::Type::Vessel) {
		ImGui::Begin("Orbit");
		const auto vessel = std::static_pointer_cast<Vessel>(ptr);
		{
			ImGui::Text("Kepler Params");
			auto const& orbit = vessel->getOrbit();
			ImGui::Text("SMA: %f", orbit.semi_major_axis);
			ImGui::Text("e: %f", orbit.eccentricity);
			ImGui::Text("MA: %f", orbit.mean_anomaly * utils::radToDeg);
			ImGui::Text("arg: %f", orbit.arg_of_periapsis * utils::radToDeg);
		}
		ImGui::Separator();
		{
			ImGui::Text("From State Vector");
			auto const& vorbit = vessel->getOrbit();
			Orbit       orbit;
			orbit.setOrbitedBody(vorbit.body_name_hash);
			orbit.setFromStateVectors(vorbit.position, vorbit.velocity);
			ImGui::Text("SMA: %f", orbit.semi_major_axis);
			ImGui::Text("e: %f", orbit.eccentricity);
			ImGui::Text("MA: %f", orbit.mean_anomaly * utils::radToDeg);
			ImGui::Text("arg: %f", orbit.arg_of_periapsis * utils::radToDeg);
		}
		ImGui::Separator();
		static bool boost = false;
		ImGui::Checkbox("Boost Orbit", &boost);
		if(boost) vessel->getBody()->ApplyForceToCenter(b2Vec2(0.f, 1000000.f), true);
		ImGui::End();
	}
}

void ObjectView::render(sf::RenderTarget& target) const {
	target.setView(m_camera);
	if(m_renderTerrain) m_terrainRenderer.render(target);

	if(const auto ptr = m_focused_entity.lock(); ptr && ptr->type == Entity::Type::Vessel) {
		const auto vessel   = std::static_pointer_cast<Vessel>(ptr);
		const auto [fx, fy] = m_planetarium.getVesselPosition(vessel->id);
		const auto [x, y]   = Terrain::getChunkCoordsFromPosition({fx, fy});

		sf::CircleShape c;

		c.setPosition((fx - x * Chunk::size) * meterToPixel, (fy - y * Chunk::size) * meterToPixel);
		c.setRadius(meterToPixel / 2.f);
		c.setOrigin(c.getRadius(), c.getRadius());
		target.draw(c);
	}
}

void ObjectView::handleIOEvent(sf::Event const& event) {
	switch(event.type) {
		case sf::Event::MouseWheelScrolled:
			if(event.mouseWheelScroll.delta > 0)
				m_camera.zoom(0.9);
			else if(event.mouseWheelScroll.delta < 0)
				m_camera.zoom(1.1);
			break;
		default: break;
	}
}

void ObjectView::handleWindowResize(sf::Vector2i const& size) {
	m_camera.setSize(size.x, size.y);
}

void ObjectView::focusEntity(std::shared_ptr<Entity> entity) {
	if(!entity) {
		m_focused_entity.reset();
		return;
	}

	m_focused_entity = entity;
}
