#pragma once

#include "SFML/Graphics/VertexBuffer.hpp"

#include "constants.hpp"

#include "Camera.hpp"
#include "View.hpp"

#include "Entities/Astrobody.hpp"

class Planetarium;
struct Orbit;

class MapView final: public View {
public:
	MapView(Planetarium& planetarium);

	void init() override;

	void step() override;
	void render(sf::RenderTarget& target) const override;
	void handleIOEvent(sf::Event const& event) override;
	void handleWindowResize(sf::Vector2i const& size) override;

	void focusEntity(std::shared_ptr<Entity> entity) override;

private:
	std::vector<sf::Vertex> getOrbitVertices(Orbit const& orbit) const;

	void renderBody(sf::RenderTarget& target, std::shared_ptr<Astrobody> const& bptr) const;

private:
	Planetarium& m_planetarium;
	Camera       m_camera;
	Camera       m_cameraNoZoom;

	std::weak_ptr<Entity> m_focused_entity;

	static constexpr double renderScale = 100000.0 / physics::AU; // pixels per AU

	sf::VertexBuffer                                       m_orbitVertices;
	std::unordered_map<std::uint32_t, std::pair<int, int>> m_indexBuffer;
};
