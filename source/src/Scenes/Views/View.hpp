#pragma once

#include <memory>

#include "SFML/System/Vector2.hpp"

#include "Entities/Entity.hpp"

namespace sf {
class Event;
class RenderTarget;
} // namespace sf

class View {
public:
	virtual ~View() = default;

	virtual void init() {}

	virtual void step()                                       = 0;
	virtual void render(sf::RenderTarget& target) const       = 0;
	virtual void handleIOEvent(sf::Event const& event)        = 0;
	virtual void handleWindowResize(sf::Vector2i const& size) = 0;

	virtual void focusEntity(std::shared_ptr<Entity> entity) = 0;
};
