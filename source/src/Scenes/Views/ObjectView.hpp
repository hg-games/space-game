#pragma once

#include "Camera.hpp"
#include "Renderer/TerrainRenderer.hpp"
#include "View.hpp"

class Planetarium;

class ObjectView final: public View {
public:
	ObjectView(Planetarium& planetarium);

	void init() override;
	void step() override;
	void render(sf::RenderTarget& target) const override;
	void handleIOEvent(sf::Event const& event) override;
	void handleWindowResize(sf::Vector2i const& size) override;

	void focusEntity(std::shared_ptr<Entity> entity) override;

private:
	Planetarium&    m_planetarium;
	Camera          m_camera;
	TerrainRenderer m_terrainRenderer;
	bool            m_renderTerrain {true};

	std::weak_ptr<Entity> m_focused_entity;
};
