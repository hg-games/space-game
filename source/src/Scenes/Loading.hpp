#pragma once

#include "Base.hpp"

namespace Scene {
class Loading: public Base {
public:
	Loading(SceneStack& stack, Context context);

private:
	bool render() const override;
	bool step() override;
	bool handleIOEvent(sf::Event const& event) override;
	void handleWindowResize(sf::Vector2i const& size) override;
};
} // namespace Scene

