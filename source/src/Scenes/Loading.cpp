#include "Loading.hpp"

#include <SFML/Graphics/RenderWindow.hpp>

namespace Scene {
Loading::Loading(SceneStack& stack, Context context): Base(stack, context) {
	auto [w, h] = context.target->getSize();
	sf::Vector2i size(w, h);
	handleWindowResize(size);
}

bool Loading::render() const {
	return false;
}

bool Loading::step() {
	return false;
}

bool Loading::handleIOEvent([[maybe_unused]] sf::Event const& event) {
	return false;
}

void Loading::handleWindowResize([[maybe_unused]] sf::Vector2i const& size) {}

} // namespace Scene

