#include "Scenes/Base.hpp"
#include "SceneStack.hpp"

namespace Scene {
Context::Context(sf::RenderTarget& target): target(&target) {}

Base::Base(SceneStack& stack, Context context): m_stack(&stack), m_context(context) {}

Base::~Base() {}

void Base::handleWindowResize(sf::Vector2i const& /*size*/) {}

void Base::onLeave() {
	// do nothing by default
}

void Base::onEnter() {
	// do nothing by default
}

void Base::requestStackPush(Scene::ID sceneID) {
	m_stack->pushScene(sceneID);
}

void Base::requestStackPop() {
	m_stack->popScene();
}

void Base::requestStackClear() {
	m_stack->clearScenes();
}

Context Base::getContext() const {
	return m_context;
}
} // namespace Scene

