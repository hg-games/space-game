#include "Editor.hpp"

#include "SFML/Graphics/CircleShape.hpp"
#include "SFML/Graphics/RenderTarget.hpp"

#include "imgui.h"

#include "constants.hpp"
#include "utils.hpp"

#include "InputHandler.hpp"
#include "PrototypeRepo.hpp"

#include "Parts/Engine/LiquidFuelEngine.hpp"
#include "Parts/Structural/Truss.hpp"

#include "Renderer/DebugRenderer.hpp"

static sf::Rect<float> getPartAABB(std::shared_ptr<Part::Base> const& e) {
	sf::Vector2f  min {}, max {};
	sf::Transform t;
	t.rotate(e->angle);
	for(auto const& lp: e->collider) {
		auto const& p = t * lp;

		min.x = std::min(min.x, p.x);
		min.y = std::min(min.y, p.y);
		max.x = std::max(max.x, p.x);
		max.y = std::max(max.y, p.y);
	}

	return sf::Rect<float> {e->offset + min, max - min};
}

namespace Scene {
Editor::Editor(SceneStack& stack, Context context):
    Base(stack, context),
    m_partsQuadtree(sf::Rect<QuadTreeFloatType>(-500, -500, 1000, 1000), getPartAABB) {
	setupFSM();
	m_camera.setCenter(0.f, 0.f);
	newEmptyVessel();

	g_debugRenderer->set_camera(&m_camera);
}

bool Editor::render() const {
	auto& target = *getContext().target;
	target.setView(m_camera);
	target.clear(sf::Color(50, 50, 50));
	renderVessel();

	if(m_handPart) {
		renderPart(target, m_handPart);
	}
	if(m_selected) {
		renderHighlightPart(target, m_selected);
	}

	return false;
}

bool Editor::step() {
	UI();

	return false;
}

bool Editor::handleIOEvent(sf::Event const& event) {
	switch(event.type) {
		case sf::Event::MouseWheelScrolled:
			if(event.mouseWheelScroll.delta > 0)
				m_camera.zoom(0.9);
			else if(event.mouseWheelScroll.delta < 0)
				m_camera.zoom(1.1);
			break;
		case sf::Event::MouseButtonPressed:
			if(event.mouseButton.button == sf::Mouse::Left) {
				m_fsm.command(
				    "cmd_left_pressed"_hs,
				    getContext().target->mapPixelToCoords(
				        sf::Vector2i(event.mouseButton.x, event.mouseButton.y), m_camera));
			}
			else if(event.mouseButton.button == sf::Mouse::Right) {
				m_fsm.command(
				    "cmd_right_pressed"_hs,
				    getContext().target->mapPixelToCoords(
				        sf::Vector2i(event.mouseButton.x, event.mouseButton.y), m_camera));
			}
			break;
		case sf::Event::MouseButtonReleased:
			if(event.mouseButton.button == sf::Mouse::Left) {
				m_fsm.command("cmd_left_released"_hs);
			}
			else if(event.mouseButton.button == sf::Mouse::Right) {
				m_fsm.command("cmd_right_released"_hs);
			}
			break;
		case sf::Event::MouseMoved:
			m_fsm.command("cmd_mouse_moved"_hs,
			              getContext().target->mapPixelToCoords(
			                  sf::Vector2i(event.mouseMove.x, event.mouseMove.y), m_camera));
			break;
		case sf::Event::KeyPressed:
			switch(event.key.code) {
				case sf::Keyboard::D: m_fsm.command("cmd_rotate_cw"_hs); break;
				case sf::Keyboard::Q: m_fsm.command("cmd_rotate_ccw"_hs); break;
				case sf::Keyboard::R: m_fsm.command("cmd_rotate_cursor"_hs); break;
				default: break;
			}
			break;
		default: break;
	}

	m_rotateStep = sf::Keyboard::isKeyPressed(sf::Keyboard::LControl);
	return false;
}

void Editor::handleWindowResize(sf::Vector2i const& size) {
	m_camera.setSize(size.x, size.y);
	m_camera.setCenter(0.f, 0.f);
}

void Editor::setupFSM() {
	////// IDLE ///////
	m_fsm.on("idle"_hs, "cmd_left_pressed"_hs) = [this](auto&& args) {
		const auto pos = std::any_cast<sf::Vector2f>(args);
		const auto ret = m_partsQuadtree.query(pos * pixelToMeter);
		if(!ret.empty()) {
			m_selected  = ret.front();
			m_dragStart = pos;
			m_fsm.set("dragging"_hs);
		}
	};

	m_fsm.on("idle"_hs, "cmd_right_pressed"_hs) = [this](auto&& args) {
		const auto pos = std::any_cast<sf::Vector2f>(args);
		const auto ret = m_partsQuadtree.query(pos * pixelToMeter);
		if(!ret.empty()) {
			m_selected = ret.front();
			m_fsm.set("selected"_hs);
		}
		else {
			m_selected.reset();
			m_fsm.set("idle"_hs);
		}
	};

	////// PLACING ///////
	m_fsm.on("placing"_hs, "cmd_left_pressed"_hs) = [this](auto&& /* args */) {
		if(placePartInHand())
			m_fsm.set("selected"_hs);
		else
			m_fsm.set("idle"_hs);
	};

	m_fsm.on("placing"_hs, "cmd_right_released"_hs) = [this](auto&& /* args */) {
		clearHand();
		m_fsm.set("idle"_hs);
	};

	m_fsm.on("placing"_hs, "cmd_mouse_moved"_hs) = [this](auto&& args) {
		const auto pos = std::any_cast<sf::Vector2f>(args);
		snapPartToVessel(m_handPart, pos);
	};

	m_fsm.on("placing"_hs, "cmd_rotate_cw"_hs) = [this](auto&& /* arg */) {
		m_handPart->rotate(90);
	};

	m_fsm.on("placing"_hs, "cmd_rotate_ccw"_hs) = [this](auto&& /* arg */) {
		m_handPart->rotate(-90);
	};

	////// DRAGGING ///////
	m_fsm.on("dragging"_hs, "cmd_left_released"_hs) = [this](auto&& /* args */) {
		m_partsQuadtree.remove({m_selected});
		m_partsQuadtree.add({m_selected});
		m_dragNoCheck = false;

		if(m_snapContext) {
			linkMountPoints(m_snapContext->hand_mp, m_snapContext->target_mp);
			m_snapContext.reset();
		}

		m_fsm.set("selected"_hs);
	};

	m_fsm.on("dragging"_hs, "cmd_mouse_moved"_hs) = [this](auto&& args) {
		const auto pos = std::any_cast<sf::Vector2f>(args);
		if(m_dragNoCheck || utils::length(pos - m_dragStart) > 1 * meterToPixel) {
			snapPartToVessel(m_selected, pos);
			m_dragNoCheck = true;

			// part was linked but we dragged it away
			if(m_selected->is_linked) {
				for(auto& mp: *m_selected->mount_points) {
					if(mp.used) unlinkMountPoint(&mp);
				}
			}
		}
	};

	m_fsm.on("dragging"_hs, "cmd_rotate_cw"_hs) = [this](auto&& /* args */) {
		m_selected->rotate(90);
	};

	m_fsm.on("dragging"_hs, "cmd_rotate_ccw"_hs) = [this](auto&& /* args */) {
		m_selected->rotate(-90);
	};

	////// SELECTED ///////
	m_fsm.on("selected"_hs, "cmd_left_pressed"_hs) = [this](auto&& args) {
		const auto pos = std::any_cast<sf::Vector2f>(args);
		const auto ret = m_partsQuadtree.query(pos * pixelToMeter);
		if(!ret.empty()) {
			m_selected = ret.front();
			m_fsm.set("dragging"_hs);
			m_dragStart    = pos;
			m_dragIsLinked = m_selected->is_linked;
		}
		else {
			m_selected.reset();
			m_fsm.set("idle"_hs);
		}
	};

	m_fsm.on("selected"_hs, "cmd_right_pressed"_hs) = [this](auto&& args) {
		const auto pos = std::any_cast<sf::Vector2f>(args);
		const auto ret = m_partsQuadtree.query(pos * pixelToMeter);
		if(!ret.empty()) {
			m_selected = ret.front();
		}
		else {
			m_selected.reset();
			m_fsm.set("idle"_hs);
		}
	};

	m_fsm.on("selected"_hs, "cmd_rotate_cw"_hs) = [this](auto&& /* args */) {
		m_selected->rotate(90);
	};

	m_fsm.on("selected"_hs, "cmd_rotate_ccw"_hs) = [this](auto&& /* args */) {
		m_selected->rotate(-90);
	};

	m_fsm.on("selected"_hs, "cmd_rotate_cursor"_hs) = [this](auto&& /* args */) {
		m_origAngle = m_selected->angle;
		m_fsm.set("rotating"_hs);
	};

	////// ROTATING ///////
	m_fsm.on("rotating"_hs, "cmd_mouse_moved"_hs) = [this](auto&& args) {
		const auto pos   = std::any_cast<sf::Vector2f>(args);
		const auto angle = utils::angle(pos - m_selected->offset * meterToPixel) * utils::radToDeg;
		if(m_rotateStep) {
			static constexpr float arr[] = {-180.f, -157.5f, -135.f, -112.5f, -90.f, -67.5f,
			                                -45.f,  -22.5f,  0.f,    22.5f,   45.f,  67.5f,
			                                90.f,   112.5f,  135.f,  157.5f,  180.f};

			m_selected->set_rotation(*std::lower_bound(std::begin(arr), std::end(arr), angle));
		}
		else {
			m_selected->set_rotation(angle);
		}
	};

	m_fsm.on("rotating"_hs, "cmd_left_pressed"_hs) = [this](auto&& /* args */) {
		m_fsm.set("selected"_hs);
	};

	m_fsm.on("rotating"_hs, "cmd_right_pressed"_hs) = [this](auto&& /* args */) {
		m_selected->set_rotation(m_origAngle);
		m_fsm.set("selected"_hs);
	};

	m_fsm.set("idle"_hs);
}

void Editor::renderVessel() const {
	auto& target = *getContext().target;
	for(auto const& part: m_vessel->getParts()) {
		renderPart(target, part);
	}
}

void Editor::renderPart(sf::RenderTarget& target, std::shared_ptr<Part::Base> const& part) const {
	sf::RenderStates states;
	states.transform.translate(part->offset * meterToPixel);
	states.transform.rotate(part->angle);
	target.draw(part->geometry, states);

	states.transform = {};
	states.transform.translate(part->offset * meterToPixel);

	if(part->mount_points) {
		for(auto const& mp: *part->mount_points) {
			sf::CircleShape c;
			c.setFillColor(mp.used ? sf::Color {70, 70, 70} : sf::Color::Green);
			c.setRadius(0.1 * meterToPixel);
			c.setPosition(mp.offset * meterToPixel);
			c.setOrigin(c.getRadius(), c.getRadius());
			target.draw(c, states);
		}
	}
}

void Editor::renderHighlightPart(sf::RenderTarget&                  target,
                                 std::shared_ptr<Part::Base> const& part) const {
	if(part->collider.empty()) return;

	sf::VertexArray hl(sf::LineStrip);
	for(auto const& p: part->collider) {
		hl.append(sf::Vertex(p * meterToPixel, sf::Color(255, 69, 0)));
	}
	hl.append(hl[0]);

	sf::RenderStates states;
	states.transform.translate(part->offset * meterToPixel);
	states.transform.rotate(part->angle);

	target.draw(hl, states);
}

void Editor::UI() {
	ImGui::Begin("Editor");
	if(ImGui::CollapsingHeader("Parts")) {
		for(auto cat_hash: g_prototypeRepo->getCategories()) {
			auto const& cat   = g_prototypeRepo->getCategoryByName(cat_hash);
			auto const& parts = g_prototypeRepo->getPartsByCategory(cat_hash);

			if(ImGui::TreeNode(fmt::format("{}", cat.name.data()).data())) {
				for(auto const& part: parts) {
					if(ImGui::Button(part->name.data())) {
						clearHand();
						m_handPart = createPartFromPrototype(part->name_hash);
						m_fsm.set("placing"_hs);
					}
				}
				ImGui::TreePop();
			}
		}
	}
	ImGui::Separator();
	ImGui::Text("In hand: %s", m_handPart ? m_handPart->name.data() : "nothing");
	ImGui::Text("Part count: %lu", m_vessel->getPartCount());
	ImGui::Separator();
	ImGui::Text("State Machine: [%s]", m_fsm.get().str.data());
	ImGui::Text("Snap Context: %s", m_snapContext.has_value() ? "true" : "false");

	ImGui::End();
}

void Editor::newEmptyVessel() {
	m_vessel.reset(new Vessel);
}

bool Editor::placePartInHand() {
	if(!m_handPart) return false;

	// special case, first part placed
	if(m_vessel->getPartCount() == 0) {
		m_handPart->offset = {};
		m_vessel->setRootPart(m_handPart);
		m_partsQuadtree.add({m_handPart});
		m_selected = m_handPart;
		clearHand();
		return true;
	}

	// temp
	// attach to mount point
	m_vessel->attachPart(m_handPart);
	m_partsQuadtree.add({m_handPart});
	m_selected = m_handPart;
	if(m_snapContext) {
		linkMountPoints(m_snapContext->hand_mp, m_snapContext->target_mp);
		m_snapContext.reset();
	}
	else {
		g_logger->warn("Editor: snap context is empty when it shouldn't");
	}

	clearHand();
	return true;
}

void Editor::snapPartToVessel(std::shared_ptr<Part::Base> part, sf::Vector2f const& mouse_pos) {
	bool snapped = false;
	if(part->mount_points) {
		// query all other object around each mount points
		sf::FloatRect rect;
		rect.width  = 1;
		rect.height = 1;

		std::vector<std::shared_ptr<Part::Base>> res;
		for(auto const& mp: *part->mount_points) {
			if(mp.used) continue;
			const auto [x, y] = mp.offset;
			rect.left         = mouse_pos.x * pixelToMeter + (x - 0.5f);
			rect.top          = mouse_pos.y * pixelToMeter + (y - 0.5f);

			g_debugRenderer->draw_quad(sf::Vector2f(rect.left, rect.top) * meterToPixel,
			                           sf::Vector2f(rect.width, rect.height) * meterToPixel,
			                           sf::Color::Yellow);

			for(auto const& p: m_partsQuadtree.query(rect)) {
				auto const aabb = getPartAABB(p);
				g_debugRenderer->draw_quad(sf::Vector2f(aabb.left, aabb.top) * meterToPixel,
				                           sf::Vector2f(aabb.width, aabb.height) * meterToPixel,
				                           sf::Color::Magenta);
				if(p == part) continue;
				res.push_back(p);
			}
		}

		// once we have all the object, compute the distance between our mountpoint and all the
		// part's mountpoints to find out the closest
		std::pair<MountPoint*, MountPoint*> min_dist_mps;
		float                               min_dist {std::numeric_limits<float>::max()};

		// hack: set part's position to the mouse pos
		// we need this because otherwise the snaping doesn't work properly
		const auto part_offset = part->offset;
		part->offset           = mouse_pos * pixelToMeter;
		for(auto const& close_part: res) {
			const auto opt_ret = part->find_closest_mountpoint(close_part);
			if(!opt_ret) continue;
			const auto [self_index, to_index, dist] = *opt_ret;

			if(dist < min_dist) {
				min_dist     = dist;
				min_dist_mps = {&part->mount_points.value()[self_index],
				                &close_part->mount_points.value()[to_index]};
			}
		}
		// then set it back
		part->offset = part_offset;

		g_logger->info(min_dist);
		// one meter max snap
		if(min_dist < 1) {
			auto const& [self, link] = min_dist_mps;
			const auto link_ptr      = link->self.lock();
			const auto self_ptr      = self->self.lock();
			snapped                  = true;

			part->offset = link_ptr->offset + link->offset - self->offset;

			m_snapContext.emplace(SnapContext {self, link});
		}
	}
	if(!snapped) {
		part->offset = mouse_pos * pixelToMeter;
		m_snapContext.reset();
	}
}

void Editor::linkMountPoints(MountPoint* part_mp, MountPoint* vessel_mp) {
	part_mp->used  = true;
	part_mp->other = vessel_mp;
	part_mp->link  = vessel_mp->self.lock();

	vessel_mp->used  = true;
	vessel_mp->other = part_mp;
	vessel_mp->link  = part_mp->self.lock();

	part_mp->self.lock()->is_linked   = true;
	vessel_mp->self.lock()->is_linked = true;
}

void Editor::unlinkMountPoint(MountPoint* mp) {
	const auto other = mp->other;

	mp->used  = false;
	mp->other = nullptr;
	mp->link.reset();

	other->used  = false;
	other->other = nullptr;
	other->link.reset();
}

void Editor::clearHand() {
	m_handPart.reset();
}

std::shared_ptr<Part::Base> Editor::createPartFromPrototype(std::uint32_t name_hash) const {
	std::shared_ptr<Part::Base> ptr;
	const auto                  type_hash = g_prototypeRepo->getTypeHash(name_hash);

	switch(type_hash) {
		case "truss"_hs:
			ptr.reset(
			    new Part::Truss(g_prototypeRepo->getPrototypeByName<TrussPrototype>(name_hash)));
			break;
		case "liquid-fuel-engine"_hs:
			ptr.reset(new Part::LiquidFuelEngine(
			    g_prototypeRepo->getPrototypeByName<LiquidFuelEnginePrototype>(name_hash)));
			break;
		default:
			g_logger->warn("Editor::createPartFromPrototype: unhandled prototype type [{}]",
			               g_prototypeRepo->getPrototypeByName(name_hash)->type);
	}

	if(ptr->mount_points) {
		for(auto& mp: *ptr->mount_points) {
			mp.self = ptr;
		}
	}
	return ptr;
}
} // namespace Scene

