#pragma once

#include <memory>

#include <SFML/Window/Event.hpp>

#include "SceneIds.hpp"

namespace sf {
class RenderTarget;
}

class SceneStack;

namespace Scene {
class Base;
typedef std::unique_ptr<Base> Ptr;

struct Context {
	Context(sf::RenderTarget& target);

	sf::RenderTarget* target;
};

class Base {
public:
	Base(SceneStack& stack, Context context);
	virtual ~Base();

	/*
	 * if this function returns true, the scene stack will draw
	 * scenes under this one
	 * if it returns false, the scene stack will stop drawing here
	 */
	virtual bool render() const = 0;

	/*
	 * if this function returns true, the scene stack will update
	 * scenes under this one
	 * if it returns false, the scene stack will stop updating here
	 */
	virtual bool step() = 0;

	/*
	 * if this function returns true, the scene stack will handle events
	 * for the scenes under this one
	 * if it returns false, the scene stack will stop here
	 */
	virtual bool handleIOEvent(sf::Event const& event) = 0;

	/*
	 * handle a resize of the main window
	 *
	 * by default does nothing
	 */
	virtual void handleWindowResize(sf::Vector2i const& size);

	/*
	 * A.onLeave() is called when state B is pushed on top of state A
	 * A.onEnter() is called when state B is poped and we return to state A
	 *
	 * Those are NOT called when a state is created/destroyed
	 */
	virtual void onLeave();
	virtual void onEnter();

protected:
	void requestStackPush(Scene::ID sceneID);
	void requestStackPop();
	void requestStackClear();

	Context getContext() const;

private:
	SceneStack* m_stack;
	Context     m_context;
};
} // namespace Scene
