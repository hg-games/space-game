#pragma once

#include "Base.hpp"

#include "Planetarium.hpp"
#include "Views/MapView.hpp"
#include "Views/ObjectView.hpp"

namespace Scene {
class Game final: public Base {
public:
	Game(SceneStack& stack, Context context);

	bool render() const override;
	bool step() override;
	bool handleIOEvent(sf::Event const& event) override;
	void handleWindowResize(sf::Vector2i const& size) override;

private:
	Planetarium m_planetarium;

	MapView    m_view;
	ObjectView m_objView;

	View* m_currentView;
};
} // namespace Scene
