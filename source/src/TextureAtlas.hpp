#pragma once

#include "SFML/Graphics/Texture.hpp"

class TextureAtlas {
public:
	TextureAtlas(unsigned int width = 0, unsigned int height = 0);

	bool loadTexture(std::size_t, std::string_view path);
	bool pack();

	size_t             getTextureCount() const { return m_rects.size(); }
	sf::Vector2u       getAtlasSize() const { return m_atlas.getSize(); }
	sf::Texture const& getAtlasTexture() const { return m_atlas; }

	std::pair<sf::Texture const&, sf::IntRect const&> getTexture(std::size_t id) const;

private:
	unsigned int m_width, m_height;

	std::unordered_map<std::size_t, sf::IntRect> m_rects;
	sf::Texture                                  m_atlas;

	/*
	 * Textures are loaded into this map until pack is called
	 * Once pack is successfully called, this map is cleared
	 */
	std::unordered_map<std::size_t, sf::Texture> m_textures;
};
