#pragma once

#include <string>

#include "PropertyTree.hpp"

#include "hashed_string.hpp"

struct PartCategory {
	PartCategory() = default; // TODO remove

	PartCategory(PropertyTree const& ptree) {
		name      = ptree["name"].getData().as_string();
		name_hash = hashed_string::to_value(name.data());

		icon_path = ptree["icon"].getData().as_string();
	}

	std::string   name;
	std::uint32_t name_hash;

	std::string icon_path;
};
