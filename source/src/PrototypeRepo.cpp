#include "PrototypeRepo.hpp"

#include "DataLoader.hpp"
#include "PropertyTree.hpp"

#include "Prototypes/Astrobody/PlanetPrototype.hpp"
#include "Prototypes/Engine/LiquidFuelEnginePrototype.hpp"
#include "Prototypes/PlanetarySystemPrototype.hpp"
#include "Prototypes/Structural/TrussPrototype.hpp"

#include "Parts/Structural/Truss.hpp"

PrototypeRepo* g_prototypeRepo = nullptr;

void PrototypeRepo::create() {
	if(!g_prototypeRepo) g_prototypeRepo = new PrototypeRepo;
}

PrototypeRepo::PrototypeRepo() {
	using namespace std::placeholders;
#define LFUNC(prototype) std::bind(&PrototypeRepo::loadPrototype<prototype>, this, _1)

	// parts
	g_dataLoader->registerLoader("planet"_hs, LFUNC(PlanetPrototype));
	g_dataLoader->registerLoader("planetary-system"_hs, LFUNC(PlanetarySystemPrototype));
	g_dataLoader->registerLoader("liquid-fuel-engine"_hs, LFUNC(LiquidFuelEnginePrototype));
	g_dataLoader->registerLoader("truss"_hs, LFUNC(TrussPrototype));

#undef LFUNC

	// others
	g_dataLoader->registerLoader("category-definition"_hs,
	                             std::bind(&PrototypeRepo::loadCategoryDef, this, _1));
}

bool PrototypeRepo::loadCategoryDef(PropertyTree const& ptree) {
	PartCategory pc(ptree);

	if(!m_categories.try_emplace(pc.name_hash, pc).second) {
		g_logger->warn("PrototypeRepo::loadCategoryDef: detected duplicate category [{}]", pc.name);
	}
	else {
		m_category_hashes.emplace_back(pc.name_hash);
	}

	return true;
}

bool PrototypeRepo::isPart(BasePrototype const& proto) const {
	switch(proto.type_hash) {
		case "truss"_hs:
		case "liquid-fuel-engine"_hs: return true;
		default: return false;
	}
}

void PrototypeRepo::registerPrototype(std::uint32_t name_hash) {
	const auto ptr = m_prototypes_by_name_hash.at(name_hash);
	const auto p   = ptr.get();

	m_prototypes_by_type[p->type_hash].emplace_back(ptr);

	if(isPart(*p)) {
		const auto part = static_cast<const PartPrototype*>(p);
		m_parts_by_category[part->category_hash].emplace_back(
		    std::static_pointer_cast<PartPrototype>(ptr));
	}
}

std::shared_ptr<BasePrototype> PrototypeRepo::getPrototypeByName(std::uint32_t name_hash) const {
	return m_prototypes_by_name_hash.at(name_hash);
}

PrototypeRepo::ProtoVec<BasePrototype> const&
PrototypeRepo::getPrototypesByType(std::uint32_t type_hash) const {
	return m_prototypes_by_type.at(type_hash);
}

PrototypeRepo::ProtoVec<PartPrototype> const&
PrototypeRepo::getPartsByCategory(std::uint32_t cat_hash) const {
	return m_parts_by_category.at(cat_hash);
}

std::vector<std::uint32_t> const& PrototypeRepo::getCategories() const {
	return m_category_hashes;
}

PartCategory const& PrototypeRepo::getCategoryByName(std::uint32_t cat_hash) const {
	return m_categories.at(cat_hash);
}

std::uint32_t PrototypeRepo::getTypeHash(std::uint32_t name_hash) const {
	return m_prototypes_by_name_hash.at(name_hash)->type_hash;
}

void PrototypeRepo::finalizeLoading() {
	// set the orbits mu values
	for(const auto& p: getPrototypesByType("planet"_hs)) {
		const auto planet = std::static_pointer_cast<PlanetPrototype>(p);
		if(planet->orbit) {
			const auto body   = getPrototypeByName<PlanetPrototype>(planet->orbit->body_name_hash);
			planet->orbit->mu = body->mu;
		}
	}
}
