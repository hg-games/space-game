#pragma once

#include <memory>

#include "box2d/b2_world.h"

#include "Entities/Vessel.hpp"

class Terrain;

class PhysicsWorld {
public:
	PhysicsWorld(sf::Vector2<double> const& origin);

	void step();
	void setTerrain(Terrain* terrain);
	void registerVessel(std::shared_ptr<Vessel> vessel);
	void transferVesselFrom(PhysicsWorld* other, std::shared_ptr<Vessel> vessel);

	// return a pointer to the destroyed vessel
	std::shared_ptr<Vessel> destroyVessel(std::uint64_t id);

	void                setOrigin(sf::Vector2<double> const& origin);
	sf::Vector2<double> getOrigin() const { return m_origin; }

	// returns the position of a global point relative to this world's origin
	sf::Vector2f getLocalPosition(sf::Vector2<double> const& global_position) const;

	sf::Rect<std::int64_t> getAABB() const;

	b2World* getb2World() { return &m_world; }

private:
	b2World  m_world;
	Terrain* m_terrain;
	sf::Vector2<double>
	    m_origin; // position of this world in the planetarium, this is the center
	              // of the box2d world. This position will get shifted when the vessel(s) in the
	              // world go too far away from the origin (to prevent floating point errors)

	std::vector<std::shared_ptr<Vessel>> m_vessels;
};
