#include "Planetarium.hpp"

#include <algorithm>
#include <queue>

#include "box2d/b2_body.h"
#include "box2d/b2_fixture.h"
#include "box2d/b2_math.h"

#include "constants.hpp"

#include "PrototypeRepo.hpp"
#include "Prototypes/Astrobody/PlanetPrototype.hpp"
#include "Prototypes/PlanetarySystemPrototype.hpp"

#include "Entities/Astrobody/Planet.hpp"

#include "DebugInfo.hpp"
#include "Logger.hpp"
#include "utils.hpp"

Planetarium* g_planetarium {nullptr};

Planetarium::Planetarium():
    m_worldsQtree(sf::Rect<std::int64_t>(std::numeric_limits<std::int64_t>::min() / 2,
                                         std::numeric_limits<std::int64_t>::min() / 2,
                                         std::numeric_limits<std::int64_t>::max(),
                                         std::numeric_limits<std::int64_t>::max()),
                  std::bind(&PhysicsWorld::getAABB, std::placeholders::_1)) {
	if(g_planetarium != nullptr) {
		g_logger->critical("Planetarium: cannot have more than one instance of the class");
		std::abort();
	}

	g_planetarium = this;
	m_debugDraw.SetFlags(b2Draw::e_shapeBit);
}

void Planetarium::loadFromPrototype(PlanetarySystemPrototype const& prototype) {
	clear();

	// load root body
	{
		// TODO: make other astrobody prototypes
		const auto rproto = std::static_pointer_cast<PlanetPrototype>(
		    g_prototypeRepo->getPrototypeByName(prototype.root));
		m_root.reset(new Planet);
		m_root->name      = rproto->name;
		m_root->name_hash = rproto->name_hash;
		m_root->color     = rproto->color;
		m_root->radius    = rproto->radius;
		m_root->mu        = rproto->mu;

		m_body_from_hash[m_root->name_hash] = m_root;
	}

	// load system's bodies
	m_bodies.reserve(prototype.bodies.size());
	for(auto body_hash: prototype.bodies) {
		const auto bproto = std::static_pointer_cast<PlanetPrototype>(
		    g_prototypeRepo->getPrototypeByName(body_hash));

		g_logger->debug("Planetarium: loading body [{}]", bproto->name);
		std::shared_ptr<Astrobody> body {nullptr};
		switch(bproto->type_hash) {
			case "planet"_hs:
				body.reset(new Planet);
				body->orbit = bproto->orbit;
				break;
			default:
				g_logger->warn(
				    "Planetarium: failed to load body [{}] from planetary system prototype [{}].",
				    bproto->name, prototype.name);
				g_logger->warn("             unhandled body type [{}]", bproto->type);
		}
		if(body) {
			body->name      = bproto->name;
			body->name_hash = bproto->name_hash;
			body->color     = bproto->color;
			body->mu        = bproto->mu;
			body->radius    = bproto->radius;
			if(body->orbit) {
				m_hierarchy[body->orbit->body_name_hash].emplace(bproto->name_hash);
			}
			m_bodies.emplace_back(std::move(body));
			m_body_from_hash[bproto->name_hash] = m_bodies.back();
		}
	}

	// create the flat hierarchy for convenience
	m_flat_hierarchy.emplace_back(m_root->name_hash);
	std::queue<std::uint32_t> to_push;
	for(const auto child_hash: m_hierarchy[m_root->name_hash])
		to_push.push(child_hash);

	while(!to_push.empty()) {
		const auto hash = to_push.front();
		to_push.pop();
		m_flat_hierarchy.emplace_back(hash);
		for(const auto child_hash: m_hierarchy[hash]) {
			to_push.push(child_hash);
		}
	}

	// set orbits mu values
	for(size_t i = 1; i < m_flat_hierarchy.size(); i++) {
		const auto hash  = m_flat_hierarchy[i];
		auto&      orbit = m_body_from_hash[hash].lock()->orbit;

		orbit->mu = m_body_from_hash.at(orbit->body_name_hash).lock()->mu;
	}

	m_positions[m_root->name_hash] = {};
	updateAstrobodiesPositions();
}

void Planetarium::step() {
	if(m_pause) return;
	m_time_since_epoch += timestep * m_warp;
	g_debugInfo->epoch = m_time_since_epoch;

	Timer::Manual timer("physics_update_time"_hs);
	timer.start();
	for(auto& world: m_physicsWorlds) {
		world.step();
	}
	timer.stop();
	g_debugInfo->physics_update_time = Timer::get_stats("physics_update_time"_hs);
	g_debugInfo->physics_world_count = m_physicsWorlds.size();

	updateAstrobodiesPositions();
	updateVesselsStateVector(timestep * m_warp);
}

void Planetarium::setWarpSpeed(int warp) {
	m_warp = std::clamp(warp, m_min_warp, m_max_warp);
}

void Planetarium::togglePause() {
	m_pause = !m_pause;
}

void Planetarium::setPause(bool pause) {
	m_pause = pause;
}

bool Planetarium::getPause() const {
	return m_pause;
}

std::shared_ptr<Astrobody> Planetarium::getBody(std::uint32_t name_hash) const {
	return m_body_from_hash.at(name_hash).lock();
}

std::shared_ptr<Astrobody> Planetarium::getRoot() const {
	return m_root;
}

std::vector<std::shared_ptr<Astrobody>> const& Planetarium::getBodies() const {
	return m_bodies;
}

sf::Vector2<double> const& Planetarium::getBodyPosition(std::uint32_t name_hash) const {
	return m_positions.at(name_hash);
}

sf::Vector2<double> const& Planetarium::getParentBodyPosition(std::uint32_t name_hash) const {
	auto bptr = m_body_from_hash.at(name_hash).lock();
	if(!bptr->orbit) return m_positions.at(m_root->name_hash);
	return m_positions.at(bptr->orbit->body_name_hash);
}

std::vector<std::shared_ptr<Vessel>> const& Planetarium::getVessels() const {
	return m_vessels;
}

std::shared_ptr<Vessel> Planetarium::getVesselById(std::uint64_t id) const {
	return m_vessel_from_id.at(id);
}

std::shared_ptr<Astrobody> Planetarium::getOrbitedBody(std::uint64_t id) const {
	auto const v = m_vessel_from_id.at(id);
	if(!v->hasOrbit()) return nullptr;

	return m_body_from_hash.at(v->getOrbit().body_name_hash).lock();
}

sf::Vector2<double> const& Planetarium::getOrbitedBodyPosition(std::uint64_t id) const {
	const auto vptr = getVesselById(id);
	if(!vptr->hasOrbit()) {
		return m_positions.at(m_root->name_hash);
	}

	return getBodyPosition(vptr->getOrbit().body_name_hash);
}

sf::Vector2<double> const& Planetarium::getVesselPosition(std::uint64_t id) const {
	return m_vessel_state_vectors.at(id).first;
}

sf::Vector2<double> const& Planetarium::getVesselVelocity(std::uint64_t id) const {
	return m_vessel_state_vectors.at(id).second;
}

sf::Vector2<double> Planetarium::getGlobalVesselPosition(std::uint64_t id) const {
	return getVesselPosition(id) + getOrbitedBodyPosition(id);
}

void Planetarium::insertVessel(std::shared_ptr<Vessel> vessel) {
	m_vessels.push_back(vessel);
	m_vessel_from_id[vessel->id] = vessel;

	if(!vessel->hasOrbit()) return;

	auto& orbit = vessel->getOrbit();

	const auto sv                      = orbit.getStateVectors(0);
	m_vessel_state_vectors[vessel->id] = sv;
	orbit.position                     = sv.first;
	orbit.velocity                     = sv.second;

	const auto pos =
	    m_vessel_state_vectors.at(vessel->id).first + m_positions.at(orbit.body_name_hash);

	// look for a physics world 100 meters around the ship
	const sf::Rect<std::int64_t> rect(pos.x - 50, pos.y - 50, 100, 100);
	const auto                   ret = m_worldsQtree.query(rect);

	PhysicsWorld* world {nullptr};
	if(ret.empty())
		world = createPhysicsWorld(pos);
	else
		world = ret.front();

	world->registerVessel(vessel);
}

PhysicsWorld* Planetarium::createPhysicsWorld(sf::Vector2<double> const& origin) {
	auto* world = &m_physicsWorlds.emplace_back(origin);
	world->setOrigin(origin);
	world->getb2World()->SetDebugDraw(&m_debugDraw);
	return world;
}

void Planetarium::updateAstrobodiesPositions() {
	// first body is the root so it has no orbit
	for(size_t i = 1; i < m_flat_hierarchy.size(); i++) {
		const auto  hash  = m_flat_hierarchy[i];
		const auto& orbit = m_body_from_hash[hash].lock()->orbit;
		m_positions[hash] =
		    orbit->getStateVectors(m_time_since_epoch).first + m_positions[orbit->body_name_hash];
	}
}

void Planetarium::updateVesselsStateVector(double step) {
	for(auto const& vessel: m_vessels) {
		if(!vessel->hasOrbit()) continue;
		Orbit& orbit = vessel->getOrbit();
		orbit.epoch += step;
		auto sv = orbit.getStateVectors(orbit.epoch);

		if(orbit.apply_shift) {
			sv.first += orbit.position_shift;
			sv.second += orbit.velocity_shift;
			orbit.setFromStateVectors(sv.first, sv.second);
			orbit.apply_shift = false;
		}
		m_vessel_state_vectors[vessel->id] = sv;
		orbit.position                     = sv.first;
		orbit.velocity                     = sv.second;
	}
}

void Planetarium::clear() {
	m_root.reset();
	m_bodies.clear();
	m_body_from_hash.clear();
	m_hierarchy.clear();
	m_positions.clear();

	m_vessels.clear();
	m_vessel_from_id.clear();
	m_vessel_state_vectors.clear();
}
