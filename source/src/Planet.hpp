#pragma once

#include "SFML/System/Vector2.hpp"

class Planet {
public:
	/*
	 * Create a planet with the initial position defined by:
	 *  - p0 initial position
	 *  - v0 initial velocity
	 *  - mu standard gravitational parameter for the central body
	 */
	Planet(sf::Vector2f const& p0, sf::Vector2f const& v0, float mu);

	void setOrbitFromInitialState(sf::Vector2f const& p0, sf::Vector2f const& v0, float mu);

	void imgui_param_debug() const;

	// orbit parameters
	float m_semimajor {0.f};
	float m_semiminor {0.f};
	float m_eccentricity {0.f};
	float m_true_anomaly {0.f};

	// initial parameters
	sf::Vector2f m_p0 {};
	sf::Vector2f m_v0 {};
	float        m_mu {0.f};

	// planet parameters
	float m_mass {0.f};
	float m_radius {0.f};
};
