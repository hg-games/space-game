#pragma once

#include <list>
#include <memory>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "Entities/Astrobody.hpp"
#include "Entities/Vessel.hpp"
#include "PhysicsDebugDraw.hpp"
#include "PhysicsWorld.hpp"
#include "QuadTree.hpp"

struct PlanetarySystemPrototype;

// holds all the information related to the solar system and the vessels currently flying
class Planetarium {
public:
	Planetarium();

	void loadFromPrototype(PlanetarySystemPrototype const& prototype);

	// updates all the orbits
	void step();

	double getTimeSinceEpoch() const { return m_time_since_epoch; }

	int  getWarpSpeed() const { return m_warp; }
	void setWarpSpeed(int warp);
	void togglePause();
	void setPause(bool pause = true);
	bool getPause() const;

	std::shared_ptr<Astrobody>                     getBody(std::uint32_t name_hash) const;
	std::shared_ptr<Astrobody>                     getRoot() const;
	std::vector<std::shared_ptr<Astrobody>> const& getBodies() const;

	sf::Vector2<double> const& getBodyPosition(std::uint32_t name_hash) const;
	sf::Vector2<double> const& getParentBodyPosition(std::uint32_t name_hash) const;

	std::vector<std::shared_ptr<Vessel>> const& getVessels() const;
	std::shared_ptr<Vessel>                     getVesselById(std::uint64_t id) const;
	std::shared_ptr<Astrobody>                  getOrbitedBody(std::uint64_t id) const;
	sf::Vector2<double> const&                  getOrbitedBodyPosition(std::uint64_t id) const;
	sf::Vector2<double> const&                  getVesselPosition(std::uint64_t id) const;
	sf::Vector2<double> const&                  getVesselVelocity(std::uint64_t id) const;
	sf::Vector2<double>                         getGlobalVesselPosition(std::uint64_t id) const;

	void insertVessel(std::shared_ptr<Vessel> vessel);

	PhysicsWorld* createPhysicsWorld(sf::Vector2<double> const& origin);

private:
	void updateAstrobodiesPositions();
	void updateVesselsStateVector(double step);
	void clear();

private:
	double    m_time_since_epoch {0.0};
	int       m_warp {1};
	const int m_min_warp {1};
	const int m_max_warp {100000000};
	bool      m_pause {false};

	/*********************
	 *	ASTROBODY DATA
	 *********************/
	std::shared_ptr<Astrobody>              m_root;
	std::vector<std::shared_ptr<Astrobody>> m_bodies;

	std::unordered_map<std::uint32_t, std::weak_ptr<Astrobody>> m_body_from_hash;

	// contains the list of direct children for a given body hash
	std::unordered_map<std::uint32_t, std::unordered_set<std::uint32_t>> m_hierarchy;
	// contains an orderd list of all bodies that garanties that parents body are always before the
	// children
	std::vector<std::uint32_t> m_flat_hierarchy;

	// contains the bodies positions relative to the system's root
	std::unordered_map<std::uint32_t, sf::Vector2<double>> m_positions;

	/*********************
	 *	VESSEL DATA
	 *********************/
	std::vector<std::shared_ptr<Vessel>>                       m_vessels;
	std::unordered_map<std::uint64_t, std::shared_ptr<Vessel>> m_vessel_from_id;
	std::unordered_map<std::uint64_t, std::pair<sf::Vector2<double>, sf::Vector2<double>>>
	    m_vessel_state_vectors;

	/*********************
	 *	PHYSICS
	 *********************/
	std::list<PhysicsWorld> m_physicsWorlds;
	Quadtree<PhysicsWorld*, sf::Rect<std::int64_t>(PhysicsWorld const*), std::int64_t>
	                 m_worldsQtree;
	PhysicsDebugDraw m_debugDraw;
};

extern Planetarium* g_planetarium;
