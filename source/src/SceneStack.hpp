#pragma once

#include <future>
#include <unordered_set>

#include "SceneIds.hpp"
#include "Scenes/Base.hpp"

class SceneStack {
public:
	enum class Action { Push, Pop, Clear };

public:
	SceneStack(Scene::Context context);
	~SceneStack();

	template<typename T>
	void registerScene(Scene::ID stateID, bool loading = false);

	void step();
	void render() const;
	void handleIOEvent(sf::Event const& event);

	void pushScene(Scene::ID stateID);
	void popScene();
	void clearScenes();

	bool isEmpty() const;

	void handleWindowResize(sf::Vector2i const& size);

	void applyPendingChanges();

private:
	Scene::Ptr createScene(Scene::ID stateID);

private:
	struct PendingChange {
		PendingChange(Action action, Scene::ID stateID = Scene::ID::None);

		Action    action;
		Scene::ID stateID;
	};

private:
	std::vector<Scene::Ptr>    m_stack;
	std::vector<PendingChange> m_pendingList;

	Scene::Context                                             m_context;
	std::unordered_map<Scene::ID, std::function<Scene::Ptr()>> m_factories;
	std::unordered_set<Scene::ID> m_loading; // states that need a loading screen
	std::future<void>             m_loadingFuture;
	std::unique_ptr<Scene::Base>  m_loadingState;
	/*
	 * if the window is resized while a scene is being loaded
	 * save it for latter and apply it once the scene is loaded
	 */
	sf::Vector2i m_lastResize;
};

template<typename T>
void SceneStack::registerScene(Scene::ID stateID, bool loading) {
	if(loading) m_loading.insert(stateID);
	m_factories[stateID] = [this]() { return Scene::Ptr(new T(*this, m_context)); };
}
