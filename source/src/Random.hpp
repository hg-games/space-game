#ifndef DEF_RANDOM_HPP
#define DEF_RANDOM_HPP

#include <random>
#include <string>

class Random {
public:
	static void          seed(std::uint64_t seed);
	static std::uint64_t getSeed();

	static std::mt19937_64 const& generator();
	static std::uint64_t          integer();
	static double                 real();

	static std::string
	string(std::uint32_t length,
	       std::string_view = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890");
};

#endif
