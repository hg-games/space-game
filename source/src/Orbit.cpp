#include "Orbit.hpp"

#include <cmath>

#include "glm/geometric.hpp"
#include "glm/trigonometric.hpp"
#include "glm/vec2.hpp"
#include "glm/vec3.hpp"

#include "PrototypeRepo.hpp"
#include "Prototypes/Astrobody/PlanetPrototype.hpp"
#include "utils.hpp"

void Orbit::setFromStateVectors(sf::Vector2<double> const& pos, sf::Vector2<double> const& vel) {
	const auto r = glm::dvec3(pos.x, pos.y, 0.0);
	const auto v = glm::dvec3(vel.x, vel.y, 0.0);

	const auto h = glm::cross(r, v);
	const auto e = glm::cross(v, h) / mu - glm::normalize(r);

	auto ta = glm::acos(glm::dot(e, r) / (glm::length(e) * glm::length(r)));
	if(glm::dot(r, v) < 0.0) ta = 2.0 * pi - ta;

	eccentricity    = glm::length(e);
	semi_major_axis = 1.0 / (2.0 / glm::length(r) - glm::length(v) * glm::length(v) / mu);
	epoch           = 0.0;

	if(eccentricity < 0.000001) {
		mean_anomaly     = std::acos(glm::dot(glm::dvec3(1.0, 0.0, 0.0), r) / glm::length(r));
		arg_of_periapsis = 0.0;
	}
	else {
		if(h.z >= 0.0)
			arg_of_periapsis = std::atan2(e.y, e.x);
		else
			arg_of_periapsis = 2.0 * pi - std::atan2(e.y, e.x);
		const auto E = 2.0
		               * std::atan(std::tan(ta / 2.0)
		                           / std::sqrt((1.0 + eccentricity) / (1.0 - eccentricity)));
		mean_anomaly = E - eccentricity * std::sin(E);
	}
}

std::pair<sf::Vector2<double>, sf::Vector2<double>> Orbit::getStateVectors(double time) const {
	double ma = mean_anomaly;
	if(time != 0.f) {
		ma = mean_anomaly
		     + time * std::sqrt(mu / (semi_major_axis * semi_major_axis * semi_major_axis));
	}

	ma = std::fmod(ma, 2 * M_PI);
	if(ma < 0.0) ma += 2 * M_PI;

	auto eps3 = [](double e, double M, double x) {
		const auto t1 = std::cos(x);
		const auto t2 = e * t1 - 1;
		const auto t3 = std::sin(x);
		const auto t4 = e * t3;
		const auto t5 = -x + t4 + M;
		const auto t6 = t5 / (0.5 * t5 * t4 / t2 + t2);
		return t5 / ((0.5 * t3 - 1.0 / 6.0 * t1 * t6) * e * t6 + t2);
	};

	// solve M = E - e*sinE
	double       E   = ma;
	double       E0  = E;
	double       dE  = 1;
	const double tol = 1e-14;
	while(dE > tol) {
		E  = E0 - eps3(eccentricity, ma, E0);
		dE = std::abs(E - E0);
		E0 = E;
	}

	const double ta = 2.0
	                  * std::atan2(std::sqrt(1 + eccentricity) * std::sin(E / 2.0),
	                               std::sqrt(1 - eccentricity) * std::cos(E / 2.0));
	const double dist = semi_major_axis * (1 - eccentricity * std::cos(E));
	const double vel  = std::sqrt(mu * semi_major_axis) / dist;

	sf::Vector2<double> r {dist * std::cos(ta), dist * std::sin(ta)};
	sf::Vector2<double> v {-vel * std::sin(E),
	                       vel * std::sqrt(1 - eccentricity * eccentricity) * std::cos(E)};

	sf::Vector2<double> rr {r.x * std::cos(arg_of_periapsis) - r.y * std::sin(arg_of_periapsis),
	                        r.x * std::sin(arg_of_periapsis) + r.y * std::cos(arg_of_periapsis)};
	sf::Vector2<double> vv {v.x * std::cos(arg_of_periapsis) - v.y * std::sin(arg_of_periapsis),
	                        v.x * std::sin(arg_of_periapsis) + v.y * std::cos(arg_of_periapsis)};

	return {rr, vv};
}

void Orbit::setOrbitedBody(std::uint32_t name_hash) {
	const auto proto = g_prototypeRepo->getPrototypeByName<PlanetPrototype>(name_hash);
	body_name_hash   = name_hash;
	body             = proto->name;
	mu               = proto->mu;
}

double Orbit::period() const {
	return 2 * M_PI * std::sqrt(semi_major_axis * semi_major_axis * semi_major_axis / mu);
}
