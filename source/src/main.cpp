#include "Application.hpp"

#include <sstream>

#include "DebugInfo.hpp"
#include "Logger.hpp"
#include "Random.hpp"
#include "StackTrace.hpp"

#include "spdlog/sinks/ringbuffer_sink.h"
#include "spdlog/sinks/stdout_color_sinks.h"

StackTrace st;
DebugInfo* g_debugInfo = new DebugInfo;

const std::shared_ptr<spdlog::logger>              g_logger = spdlog::stdout_color_mt("console");
std::shared_ptr<spdlog::sinks::ringbuffer_sink_mt> g_logger_ringbuffer;

int main() {
	g_logger->sinks().emplace_back(new spdlog::sinks::ringbuffer_sink_mt(1000));
	g_logger_ringbuffer =
	    std::static_pointer_cast<spdlog::sinks::ringbuffer_sink_mt>(g_logger->sinks().back());

	g_logger->set_pattern("[%H:%M:%S.%e] [%L] %v");
	g_logger->set_level(spdlog::level::debug);

	g_logger->info("Starting...");

	const auto seed = std::time(nullptr);
	g_logger->info("Initalizing random module with seed [{}]", seed);
	Random::seed(seed);

	Application app;
	app.run();

	g_logger->info("Done");
}
