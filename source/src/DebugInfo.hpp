#pragma once

#include <array>
#include <cstdint>

struct DebugInfo {
	std::array<float, 3> ups {};
	std::array<float, 3> fps {};
	std::array<float, 3> render_time {};
	std::array<float, 3> physics_update_time {};

	std::size_t frame_count {};

	std::size_t debug_vertex_count {};

	std::uint32_t rendered_chunks {};
	int           physics_world_count {0};
	double        epoch {0};
};

extern DebugInfo* g_debugInfo;
