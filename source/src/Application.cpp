#include "Application.hpp"

#include <GL/gl.h>

#include <chrono>
#include <thread>

#include "SFML/Graphics/Text.hpp"
#include "SFML/Window/Event.hpp"

#include "DataLoader.hpp"
#include "DebugInfo.hpp"
#include "InputHandler.hpp"
#include "Logger.hpp"
#include "PrototypeRepo.hpp"
#include "ResourceHandler.hpp"
#include "TextureAtlas.hpp"

#include "Scenes/Editor.hpp"
#include "Scenes/Game.hpp"

#include "Renderer/DebugRenderer.hpp"

#include "hashed_string.hpp"
#include "utils.hpp"

#include "imgui-SFML.h"
#include "imgui.h"

Application::Application(): m_sceneStack(m_window) {
	InputHandler::create();
	ResourceHandler::create();
	DebugRenderer::create();
	DataLoader::create();
	PrototypeRepo::create();

	g_resources->loadDefaultFont("data/fonts/NotoSansMono-Regular.ttf");

	auto mode = sf::VideoMode::getDesktopMode();
	g_logger->info("Detected video mode {}x{} {}bpp", mode.width, mode.height, mode.bitsPerPixel);
	m_window.create(mode, "game02", sf::Style::Titlebar);
	m_window.setPosition(sf::Vector2i(mode.width, mode.height) / 2
	                     - sf::Vector2i(m_window.getSize()) / 2);
	// m_window.setVerticalSyncEnabled(true);

	m_defaultView = m_window.getView();

	const auto settings = m_window.getSettings();
	g_logger->info("OpenGL Version {}.{}", settings.majorVersion, settings.minorVersion);
	g_logger->info("OpenGL Vendor: {}", glGetString(GL_VENDOR));
	g_logger->info("OpenGL Renderer: {}", glGetString(GL_RENDERER));

	ImGui::SFML::Init(m_window);
	ImGui::SetWindowFocus(nullptr);

	g_dataLoader->loadMods();
	g_dataLoader->loadDefinitions();

	registerScenes();
	m_sceneStack.pushScene(Scene::ID::Game);
	m_sceneStack.applyPendingChanges();
}

void Application::run() {
	g_debugInfo->frame_count = 0;

	sf::Clock clock;
	while(m_window.isOpen()) {
		auto fps = Timer::Manual("fps"_hs);
		fps.start();
		const auto frame_start = utils::time::now();

		// clear here so it can be used everywhere
		g_debugRenderer->clear();

		sf::Event event;
		while(m_window.pollEvent(event)) {
			ImGui::SFML::ProcessEvent(event);
			auto const& io = ImGui::GetIO();
			if(io.WantCaptureMouse) {
				continue;
			}

			g_inputHandler->handleEvent(event);
			switch(event.type) {
				case sf::Event::Closed: close(); break;
				case sf::Event::Resized:
					m_sceneStack.handleWindowResize(
					    sf::Vector2i(event.size.width, event.size.height));
					m_defaultView.setSize(event.size.width, event.size.height);
					m_defaultView.setCenter(event.size.width / 2, event.size.height / 2);
					break;
				case sf::Event::KeyPressed:
					if(event.key.code == sf::Keyboard::Escape)
						close();
					else
						m_sceneStack.handleIOEvent(event);
					break;
				default: m_sceneStack.handleIOEvent(event); break;
			}
		}

		{
			auto ups = Timer::Scoped("ups"_hs);
			g_inputHandler->update(m_window);
			ImGui::SFML::Update(m_window, clock.restart());
			step();
		}
		g_debugInfo->ups = Timer::get_stats("ups"_hs);

		{
			auto rt = Timer::Scoped("render_time"_hs);
			render();
		}
		g_debugInfo->render_time = Timer::get_stats("render_time"_hs);

		g_debugInfo->frame_count++;

		const auto frame_end  = utils::time::now();
		const auto frame_time = frame_end - frame_start;
		const auto diff =
		    std::chrono::duration<decltype(frame_time)::rep, std::ratio<1, 60>>(1) - frame_time;
		if(diff.count() > 0) {
			std::this_thread::sleep_for(diff);
		}
		fps.stop();
		g_debugInfo->fps = Timer::get_stats("fps"_hs);
	}

	g_logger->info("Main loop exists at tick {}", g_debugInfo->frame_count);
	ImGui::SFML::Shutdown();
}

void Application::step() {
	m_sceneStack.step();

	ImGui::SetNextWindowPos({0.f, 0.f});
	ImGui::Begin("Debug");
	ImGui::Text("fps/ups %.1f/%.1f", 1000.f / g_debugInfo->fps[0], 1000.f / g_debugInfo->ups[0]);
	ImGui::Text("Update time(ms): %4.1f/%4.1f/%4.1f", g_debugInfo->ups[0], g_debugInfo->ups[1],
	            g_debugInfo->ups[2]);
	ImGui::Text("Render time(ms): %4.1f/%4.1f/%4.1f", g_debugInfo->render_time[0],
	            g_debugInfo->render_time[1], g_debugInfo->render_time[2]);
	ImGui::Text("Physics time(ms): %4.1f/%4.1f/%4.1f", g_debugInfo->physics_update_time[0],
	            g_debugInfo->physics_update_time[1], g_debugInfo->physics_update_time[2]);
	ImGui::Separator();
	ImGui::Text("DebugRenderer: %lu vertices", g_debugInfo->debug_vertex_count);
	ImGui::Text("Physics worlds: %d", g_debugInfo->physics_world_count);
	ImGui::Separator();
	ImGui::Text("Epoch: %.2fs", g_debugInfo->epoch);
	ImGui::End();

	ImGui::Begin("Console");
	{
		static constexpr const char* log_modes[] = SPDLOG_LEVEL_NAMES;
		static int                   selected {spdlog::level::debug};

		auto const& msg =
		    g_logger_ringbuffer->last_formatted(spdlog::level::from_str(log_modes[selected]), 100);

		ImGui::Text("Log mode");
		ImGui::SameLine();
		ImGui::SetNextItemWidth(100);
		ImGui::Combo("##Mode", &selected, log_modes, std::size(log_modes));
		ImGui::Separator();
		ImGui::BeginChild("Log", ImVec2(0, -(ImGui::GetFrameHeightWithSpacing())));
		for(auto it = msg.crbegin(); it != msg.crend(); ++it) {
			ImGui::TextUnformatted(it->data(), it->data() + it->size());
		}
		ImGui::EndChild();
		ImGui::Separator();
		ImGui::Text("Logged messages: %lu", g_logger->logged_count());
	}
	ImGui::End();
}

void Application::render() {
	m_window.clear();
	m_window.setView(m_defaultView);
	m_sceneStack.render();
	g_debugRenderer->render(m_window);

	ImGui::SFML::Render(m_window);
	m_window.display();
}

void Application::close() {
	m_window.close();
}

void Application::registerScenes() {
	m_sceneStack.registerScene<Scene::Game>(Scene::ID::Game, false);
	m_sceneStack.registerScene<Scene::Editor>(Scene::ID::Editor, false);
}
