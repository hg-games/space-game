#include "MountPoint.hpp"

#include "Logger.hpp"
#include "Parts/Base.hpp"
#include "utils.hpp"

float MountPoint::distance(MountPoint const& l_other) const {
	if(!self.lock()) g_logger->warn("MountPoint::distance: self is null");
	if(!l_other.self.lock()) g_logger->warn("MountPoint::distance: other is null");
	return utils::length(self.lock()->offset + offset
	                     - (l_other.self.lock()->offset + l_other.offset));
}
