#pragma once

#include "SFML/System/Vector2.hpp"

#include <cstdint>
#include <memory>

namespace Part {
struct Base;
}

struct MountPoint {
	enum ResourcesTransferFlagBits : std::uint16_t {
		NONE  = 0,
		FUEL  = 1 << 0,
		POWER = 1 << 1,
		HEAT  = 1 << 2,
		CREW  = 1 << 3,

		COUNT = 1 << 4,
	};
	using ResourcesTranferFlags = std::uint16_t;

	sf::Vector2f              local_offset; // offset in local reference
	sf::Vector2f              offset;       // offset in global reference (rotated)
	bool                      used {false};
	ResourcesTranferFlags     tranfer_flags {ResourcesTransferFlagBits::NONE};
	std::weak_ptr<Part::Base> self;
	std::weak_ptr<Part::Base> link;
	MountPoint*               other;
	std::size_t               index; // index of this mount point in the part's mount point vector

	float distance(MountPoint const& l_other) const;
};

