#pragma once

#include "Parts/Base.hpp"

#include "Prototypes/Engine/LiquidFuelEnginePrototype.hpp"

namespace Part {
struct LiquidFuelEngine: public Base {
	LiquidFuelEngine(std::shared_ptr<LiquidFuelEnginePrototype> const& p): Base(p) {}
};
} // namespace Part
