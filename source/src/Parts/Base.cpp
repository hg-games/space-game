#include "Base.hpp"

#include <cmath>

#include "utils.hpp"

namespace Part {

std::optional<std::tuple<std::size_t, std::size_t, float>>
Base::find_closest_mountpoint(std::shared_ptr<Base> other) const {
	if(!mount_points || !other->mount_points) return std::nullopt;

	float       min_dist       = std::numeric_limits<float>::max();
	std::size_t min_index_from = -1;
	std::size_t min_index_to   = -1;

	for(auto const& self_mp: *mount_points) {
		if(self_mp.used) continue;

		for(auto const& other_mp: *other->mount_points) {
			if(other_mp.used) continue;

			const float d = self_mp.distance(other_mp);
			if(d < min_dist) {
				min_dist       = d;
				min_index_from = self_mp.index;
				min_index_to   = other_mp.index;
			}
		}
	}

	if(min_index_from == -1ul) return std::nullopt;
	return {{min_index_from, min_index_to, min_dist}};
}

void Base::set_rotation(float l_angle) {
	if(angle == l_angle) return;

	angle = l_angle;

	if(mount_points) {
		const auto cosa = std::cos(l_angle * utils::degToRad);
		const auto sina = std::sin(l_angle * utils::degToRad);
		for(auto& mp: *mount_points) {
			const auto [x, y] = mp.local_offset;

			mp.offset.x = x * cosa - y * sina;
			mp.offset.y = x * sina + y * cosa;
		}
	}
}

void Base::rotate(float l_angle) {
	set_rotation(angle + l_angle);
}

} // namespace Part
