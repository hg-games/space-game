#include "Parts/Base.hpp"

#include "Prototypes/Structural/TrussPrototype.hpp"

namespace Part {
struct Truss: public Base {
	Truss(std::shared_ptr<TrussPrototype> const& p): Base(p) {}
};
} // namespace Part
