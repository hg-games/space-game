#pragma once

#include <memory>

#include "SFML/Graphics/VertexArray.hpp"

#include "Parts/details/MountPoint.hpp"
#include "Prototypes/PartPrototype.hpp"

#include "constants.hpp"

namespace Part {
struct Base {
	Base(std::shared_ptr<PartPrototype> const& p):
	    name_hash(p->name_hash), name(p->name), geometry(sf::TriangleFan) {
		collider = p->geometry;
		collider.shrink_to_fit();
		for(auto const& v: p->geometry) {
			geometry.append(sf::Vertex(v * meterToPixel, sf::Color(0, 0, 255, 127)));
		}
		geometry.append(geometry[0]);

		if(p->mount_points) {
			mount_points = p->mount_points;
		}
	}

	std::uint32_t name_hash;
	std::string   name;

	std::optional<std::vector<MountPoint>> mount_points;
	std::vector<sf::Vector2f>              collider;
	bool                                   is_linked {false}; // is this part linked to another?

	// TODO: change this to a better rendering method
	sf::VertexArray geometry;
	sf::Vector2f    offset;
	float           angle {0.f};

	std::optional<std::tuple<std::size_t, std::size_t, float>>
	find_closest_mountpoint(std::shared_ptr<Base> other) const;

	void set_rotation(float l_angle);
	void rotate(float l_angle);
};
} // namespace Part

