#ifndef DEF_APPLICATION_HPP
#define DEF_APPLICATION_HPP

#include "SFML/Graphics/RenderWindow.hpp"

#include "SceneStack.hpp"

class Application {
public:
	Application();

	void run();

private:
	void step();
	void render();
	void close();

	void registerScenes();

private:
	sf::View         m_defaultView;
	sf::RenderWindow m_window;
	SceneStack       m_sceneStack;
};

#endif
