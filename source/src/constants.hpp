#pragma once

// box2d
constexpr float meterToPixel = 32.f;
constexpr float pixelToMeter = 1.f / meterToPixel;

// maths
constexpr float pi = 3.141592653589793f;

namespace physics {
constexpr double G       = 6.67430e-11;    // gravitational constant m3 kg-1 s-2
constexpr double AU      = 1.495978707e11; // astronomical unit
constexpr float  simSize = 2000.f; // size of a physics simulation, vessel going further than this
                                   // get unloaded from the simulation
} // namespace physics

// general
constexpr float timestep = 1.f / 60.f;
