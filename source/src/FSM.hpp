#pragma once

#include <any>

#include <functional>
#include <string_view>
#include <unordered_map>

#include "Logger.hpp"
#include "hashed_string.hpp"
#include "utils.hpp"

class FSM {
public:
	using Args = std::any;

	struct State {
		std::size_t      hash;
		std::string_view str;

		constexpr State(hashed_string hstr): hash {hstr.value()}, str {hstr} {}

		constexpr operator std::size_t() const { return hash; }
	};

	using Trigger = State;

private:
	using Bistate  = std::pair<std::size_t, std::size_t>;
	using Callback = std::function<void(Args const& args)>;

public:
	FSM(): current_state("__internal__invalid__") {}

	Callback& on(State const& from, Trigger const& trigger) {
		return m_callbacks[Bistate(from, trigger)];
	}

	void command(State const& cmd, Args const& args = {}) {
		auto bis = Bistate(current_state, cmd);

		auto it = m_callbacks.find(bis);
		if(it == m_callbacks.end()) {
			g_logger->debug("FSM: unhandled transition [{}] - [{}]", current_state.str, cmd.str);
			return;
		}

		it->second(args);
	}

	void set(State const& state) { current_state = state; }

	State get() const { return current_state; }

private:
	std::unordered_map<Bistate, Callback, pair_hash> m_callbacks;
	State                                            current_state;
};
