#pragma once

#include <functional>
#include <string_view>
#include <unordered_map>

#include "hashed_string.hpp"

struct lua_State;
class PropertyTree;

class DataLoader {
public:
	using LoaderFunction = std::function<bool(PropertyTree const&)>;

	static void create();
	~DataLoader();

	void registerLoader(hashed_string::hash_type type_hash, LoaderFunction func);

	// find and load all the mods
	void loadMods();

	// once all the mods are loaded, load the definition from the "data"
	// table contained in the Lua State
	void loadDefinitions();

private:
	DataLoader();

	void load_mod(std::string_view path);

private:
	lua_State* L {nullptr};
	bool       m_mods_loaded {false};

	std::unordered_map<hashed_string::hash_type, LoaderFunction> m_loaders;
};

extern DataLoader* g_dataLoader;
