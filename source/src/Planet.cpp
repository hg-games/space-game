#include "Planet.hpp"

#include "glm/geometric.hpp"
#include "glm/trigonometric.hpp"
#include "glm/vec2.hpp"
#include "glm/vec3.hpp"

#include "constants.hpp"
#include "imgui.h"

Planet::Planet(sf::Vector2f const& p0, sf::Vector2f const& v0, float mu) {
	m_p0 = p0;
	m_v0 = v0;
	m_mu = mu;
}

void Planet::setOrbitFromInitialState(sf::Vector2f const& p0, sf::Vector2f const& v0, float mu) {
	const auto r = glm::vec3(p0.x, p0.y, 0.f);
	const auto v = glm::vec3(v0.x, v0.y, 0.f);

	const auto h = glm::cross(r, v);
	const auto e = glm::cross(v, h) / mu - glm::normalize(r);

	auto ta = glm::acos(glm::dot(e, r) / (glm::length(e) * glm::length(r)));
	if(glm::dot(r, v) < 0.f) ta = 2 * pi - ta;

	m_eccentricity = glm::length(e);
	m_semimajor    = 1.f / (2 / glm::length(r) - glm::length(v) * glm::length(v) / mu);
	m_semiminor    = m_semimajor * std::sqrt(1 - m_eccentricity * m_eccentricity);
	m_true_anomaly = ta;
	m_p0           = p0;
	m_v0           = v0;
	m_mu           = mu;
}

void Planet::imgui_param_debug() const {
	ImGui::Text("Eccentricity: %f", m_eccentricity);
	ImGui::Text("SemiMajor Axis: %f", m_semimajor);
	ImGui::Text("True Anomaly: %f°", glm::degrees(m_true_anomaly));
}
