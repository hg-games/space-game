#pragma once

#include "SFML/System/Vector2.hpp"

#include "Prototypes/PartPrototype.hpp"

struct LiquidFuelEnginePrototype: public PartPrototype {
	LiquidFuelEnginePrototype(PropertyTree const& ptree):
	    PartPrototype(ptree, PrototypeType::LiquidFuelEngine) {

		thrust = ptree["thrust"].getData().as_number();
		for(auto const& f: ptree["fuel"].getData().as_vector())
			fuel.emplace_back(f.as_string());
		for(auto const& fr: ptree["fuel_ratio"].getData().as_vector())
			fuel_ratio.emplace_back(fr.as_number());
		fuel_consumption = ptree["fuel_consumption"].getData().as_number();
	}

	float                    thrust;
	sf::Vector2f             size;
	std::vector<std::string> fuel;
	std::vector<float>       fuel_ratio;
	float                    fuel_consumption;
};
