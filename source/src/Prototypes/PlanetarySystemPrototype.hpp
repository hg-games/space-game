#pragma once

#include "BasePrototype.hpp"

struct PlanetarySystemPrototype: public BasePrototype {
	PlanetarySystemPrototype(PropertyTree const& ptree):
	    BasePrototype(ptree, PrototypeType::PlanetarySystem) {

		const auto& bdata = ptree["bodies"].getData().as_vector();
		bodies.reserve(bdata.size());
		for(auto const& b: bdata) {
			bodies.emplace_back(hashed_string::to_value(b.as_string().data()));
		}

		root = hashed_string::to_value(ptree["root"].getData().as_string().data());
	}

	std::vector<std::uint32_t> bodies;
	std::uint32_t              root;
};
