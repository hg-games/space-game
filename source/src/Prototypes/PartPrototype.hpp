#pragma once

#include <optional>

#include "BasePrototype.hpp"
#include "Parts/details/MountPoint.hpp"

#include "SFML/System/Vector2.hpp"

struct PartPrototype: public BasePrototype {
	PartPrototype(PropertyTree const& ptree, PrototypeType ctype): BasePrototype(ptree, ctype) {
		category      = ptree["category"].getData().as_string();
		category_hash = hashed_string::to_value(category.data());

		auto const& g = ptree["geometry"].getData().as_vector();
		for(size_t i = 1; i < g.size(); i += 2) {
			const double x = g.at(i - 1).as_number();
			const double y = g.at(i).as_number();

			geometry.emplace_back(x, y);
		}

		if(ptree.has("mount_points")) {
			mount_points.emplace();
			auto const& points = ptree["mount_points"].getData().as_vector();
			for(size_t i = 1; i < points.size(); i += 2) {
				const float x = points.at(i - 1).as_number();
				const float y = points.at(i).as_number();

				mount_points->emplace_back();
				auto& mp        = mount_points->back();
				mp.local_offset = {x, y};
				mp.offset       = {x, y};
				mp.index        = i / 2;
			}
		}
	}

	std::string              category;
	hashed_string::hash_type category_hash;

	std::vector<sf::Vector2f>              geometry;
	std::optional<std::vector<MountPoint>> mount_points;
};
