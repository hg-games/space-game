#pragma once

#include <optional>

#include "SFML/Graphics/Color.hpp"

#include "Orbit.hpp"
#include "Prototypes/BasePrototype.hpp"

#include "constants.hpp"

struct PlanetPrototype: public BasePrototype {
	PlanetPrototype(PropertyTree const& ptree): BasePrototype(ptree, PrototypeType::Planet) {
		radius = ptree["radius"].getData().as_number();
		// prefer mu over mass because it's more precise (usually)
		if(ptree.has("mu")) {
			mu   = ptree["mu"].getData().as_number();
			mass = mu / physics::G;
		}
		else {
			mass = ptree["mass"].getData().as_number();
			mu   = physics::G * mass;
		}

		if(ptree.has("orbit")) {
			auto const& o = ptree["orbit"];
			orbit         = Orbit {};

			orbit->eccentricity     = o["eccentricity"].getData().as_number();
			orbit->semi_major_axis  = o["semi_major_axis"].getData().as_number();
			orbit->arg_of_periapsis = o["arg_of_periapsis"].getData().as_number();
			orbit->mean_anomaly     = o["mean_anomaly"].getData().as_number();

			orbit->body           = o["body"].getData().as_string();
			orbit->body_name_hash = hashed_string::to_value(orbit->body.data());
		}

		{
			const auto c = ptree["color"].getData().as_vector();
			color.r      = c[0].as_number();
			color.g      = c[1].as_number();
			color.b      = c[2].as_number();
		}
	}

	double radius;
	double mass;
	double mu;

	std::optional<Orbit> orbit {std::nullopt};

	sf::Color color; // render color when zoomed out it map view
};
