#pragma once

#include "Prototypes/PartPrototype.hpp"

struct TrussPrototype: public PartPrototype {
	TrussPrototype(PropertyTree const& ptree): PartPrototype(ptree, PrototypeType::Truss) {}
};
