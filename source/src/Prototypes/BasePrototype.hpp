#pragma once

#include <string>

#include "PropertyTree.hpp"

#include "hashed_string.hpp"

enum class PrototypeType {
	Base,

	PlanetarySystem,
	Planet,

	Truss,
	LiquidFuelEngine,
};

struct BasePrototype {
	BasePrototype(PropertyTree const& ptree, PrototypeType ctype): class_type(ctype) {
		name = ptree["name"].getData().as_string();
		type = ptree["type"].getData().as_string();

		name_hash = hashed_string(name.data()).value();
		type_hash = hashed_string(type.data()).value();
	}

	std::string name;
	std::string type;

	hashed_string::hash_type name_hash;
	hashed_string::hash_type type_hash;

	PrototypeType class_type;
};
