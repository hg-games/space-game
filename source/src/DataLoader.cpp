#include "DataLoader.hpp"

#include "lua.hpp"
#include "lualib/serpent.hpp"

#include "Logger.hpp"
#include "PropertyTree.hpp"
#include "ResourceHandler.hpp"
#include "TextureAtlas.hpp"
#include "hashed_string.hpp"

DataLoader* g_dataLoader = nullptr;

static void read_table(lua_State* L, int index, PropertyTree& ptree) {
	static const auto insert_data = [&](auto& tree, auto const& data) {
		if(tree.has_data()) {
			if(tree.getData().is_vector()) {
				tree.getData().as_vector().emplace_back(data);
			}
			else {
				const auto d = tree.getData();
				tree.template setData<std::vector<PropertyTree::Data>>({});
				tree.getData().as_vector().emplace_back(d);
				tree.getData().as_vector().emplace_back(data);
			}
		}
		else {
			tree.setData(data);
		}
	};
	lua_pushvalue(L, index);
	lua_pushnil(L); /* first key */
	while(lua_next(L, -2) != 0) {
		/*
		 * -1 -> value
		 * -2 -> key
		 */

		std::string_view key;
		if(lua_type(L, -2) == LUA_TSTRING) key = lua_tostring(L, -2);

		switch(lua_type(L, -1)) {
			case LUA_TTABLE: read_table(L, -1, ptree[key]); break;
			case LUA_TSTRING: insert_data(ptree[key], std::string(lua_tostring(L, -1))); break;
			case LUA_TBOOLEAN:
				insert_data(ptree[key], static_cast<bool>(lua_toboolean(L, -1)));
				break;
			case LUA_TNUMBER: insert_data(ptree[key], lua_tonumber(L, -1)); break;
			default: g_logger->warn("DataLoader: unhandled type [{}]", luaL_typename(L, -1)); break;
		}

		// remove value
		lua_pop(L, 1);
	}
	lua_pop(L, 1); // remove saved index
}

class DataLoaderTypeNotHandledException final: public std::exception {
public:
	DataLoaderTypeNotHandledException(std::string_view type) noexcept: m_type(type) {}
	~DataLoaderTypeNotHandledException() = default;

	const char* what() const noexcept { return "DataLoader: type not handled"; }

	auto type() const { return m_type; }

private:
	std::string m_type;
};

void DataLoader::create() {
	if(!g_dataLoader) g_dataLoader = new DataLoader;
}

DataLoader::DataLoader() {
	L = luaL_newstate();

	int error = 0;
	luaL_requiref(L, "_G", luaopen_base, 1);
	luaL_requiref(L, LUA_LOADLIBNAME, luaopen_package, 1);
	luaL_requiref(L, LUA_MATHLIBNAME, luaopen_math, 1);
	luaL_requiref(L, LUA_STRLIBNAME, luaopen_string, 1);
	luaL_requiref(L, LUA_TABLIBNAME, luaopen_table, 1);
	luaL_requiref(L, "serpent", luaopen_serpent, 1);

	// clear lua path
	lua_getglobal(L, "package");
	lua_pushstring(L, "");
	lua_setfield(L, -2, "path");
	lua_pop(L, 1);

	error = luaL_dofile(L, "data/core/lualib/dataloader.lua");
	if(error) {
		g_logger->error(lua_tostring(L, -1));
		lua_pop(L, 1);
	}
}

DataLoader::~DataLoader() {
	lua_close(L);
}

void DataLoader::registerLoader(hashed_string::hash_type type_hash, LoaderFunction func) {
	m_loaders[type_hash] = func;
}

void DataLoader::loadMods() {
	load_mod("data/base");
	// find and load mods from the mods directory

	m_mods_loaded = true;
}

void DataLoader::loadDefinitions() {
	if(!m_mods_loaded) {
		g_logger->error("DataLoader: loadDefinitions called before loadMods, aborting...");
		std::abort();
	}

	PropertyTree data_ptree("data");

	// push table on stack
	lua_getglobal(L, "data");
	lua_getfield(L, -1, "raw");

	read_table(L, -1, data_ptree);

	lua_pop(L, 2); // pop data + raw

	// extract data from the property tree
	for(auto const& type_ptree: data_ptree) {
		const auto type = hashed_string(type_ptree.key().data());
		g_logger->debug("Loading definitions for type [{}]", type);

		try {
			// for each data type, retrieve the proper loader
			auto const& loader = m_loaders.at(type.value());

			for(auto const& def_ptree: type_ptree) {
				try {
					g_logger->debug("\tLoading definition for [{}]", def_ptree.key());

					loader(def_ptree);
				}
				catch(PropertyTreeKeyException const& e) {
					g_logger->error(
					    "DataLoader: property [{}] was not found for prototype [{}] of type [{}]",
					    e.key(), def_ptree.key(), type);
					std::abort();
				}
			}
		}
		catch(std::out_of_range const& e) {
			g_logger->error("DataLoader: no registred loader for type [{}]", type);
			std::abort();
		}
		catch(DataLoaderTypeNotHandledException const& e) {
			g_logger->error("DataLoader: type [{}] is not known by the game.", e.type());
			std::abort();
		}
	}

	/*
	int error = luaL_dostring(L, R"(print("data.raw " .. serpent.block(data.raw)))");
	if(error) {
	    g_logger->error(lua_tostring(L, -1));
	    lua_pop(L, 1);
	}
	//*/
}

void DataLoader::load_mod(std::string_view path) {
	const auto search_path = fmt::format("./{}/?.lua", path);
	const auto data_file   = fmt::format("{}/data.lua", path);

	g_logger->info("Loading {} mod", path);

	// set the path for require to the mod dir
	lua_getglobal(L, "package");
	lua_pushstring(L, search_path.data());
	lua_setfield(L, -2, "path");
	lua_pop(L, 1);

	int error = luaL_dofile(L, data_file.data());
	if(error) {
		g_logger->error(lua_tostring(L, -1));
		lua_pop(L, 1);
	}
}
