#pragma once

namespace Scene {
enum class ID {
	None,
	Loading, // special state for loading screens
	MainMenu,
	Editor,
	Game,
};
}
