#pragma once

#include <memory>
#include <unordered_map>
#include <unordered_set>

#include "Prototypes/BasePrototype.hpp"
#include "Prototypes/PartPrototype.hpp"

#include "Logger.hpp"
#include "PartCategory.hpp"

class PropertyTree;

class PrototypeRepo {
public:
	static void create();

	template<class P>
	using ProtoVec = std::vector<std::shared_ptr<P>>;

	// helper functions
	std::shared_ptr<BasePrototype>    getPrototypeByName(std::uint32_t name_hash) const;
	ProtoVec<BasePrototype> const&    getPrototypesByType(std::uint32_t type_hash) const;
	ProtoVec<PartPrototype> const&    getPartsByCategory(std::uint32_t cat_hash) const;
	std::vector<std::uint32_t> const& getCategories() const;
	PartCategory const&               getCategoryByName(std::uint32_t cat_hash) const;
	std::uint32_t                     getTypeHash(std::uint32_t name_hash) const;

	template<class T>
	std::shared_ptr<T> getPrototypeByName(std::uint32_t name_hash) const {
		return std::static_pointer_cast<T>(getPrototypeByName(name_hash));
	}

	/*
	 * For each prototype that has a reference to another, load the proper data
	 */
	void finalizeLoading();

private:
	PrototypeRepo();

	template<class P>
	bool loadPrototype(PropertyTree const& ptree) {
		const auto prototype = new P(ptree);

		if(!m_prototypes_by_name_hash.try_emplace(prototype->name_hash, std::move(prototype))
		        .second)
			g_logger->warn("PrototypeRepo::loadPrototype: detected duplicate prototype [{}]",
			               prototype->name);
		else
			registerPrototype(prototype->name_hash);

		return true;
	}

	bool loadCategoryDef(PropertyTree const& ptree);
	bool isPart(BasePrototype const& proto) const;
	void registerPrototype(std::uint32_t name_hash);

private:
	// storage maps

	// <name hash, prototype> central storage for all prototypes
	std::unordered_map<std::uint32_t, std::shared_ptr<BasePrototype>> m_prototypes_by_name_hash;
	// <cat hash, cat> central storage for category definitions
	std::unordered_map<std::uint32_t, PartCategory> m_categories;

	// helper maps
	template<class P>
	using ProtoMap = std::unordered_map<std::uint32_t, std::vector<std::shared_ptr<P>>>;

	// <type hash, prototype_ptrs> helper map
	ProtoMap<BasePrototype> m_prototypes_by_type;
	// <cat hash, prototype_ptrs> helper map
	ProtoMap<PartPrototype> m_parts_by_category;

	std::vector<std::uint32_t> m_category_hashes;
};

extern PrototypeRepo* g_prototypeRepo;
