#include "SceneStack.hpp"

#include <cassert>

#include "Scenes/Loading.hpp"

SceneStack::SceneStack(Scene::Context context):
    m_stack(), m_pendingList(), m_context(context), m_factories() {}

SceneStack::~SceneStack() {
	/*
	 * If a state is still loading, give it 10 second before killing it
	 * TODO: fix this
	 */
	if(m_loadingFuture.valid()) {
		m_loadingFuture.wait_for(std::chrono::seconds(10));
	}
}

void SceneStack::step() {
	// a state is loading
	if(m_loadingFuture.valid()) {
		auto status = m_loadingFuture.wait_for(std::chrono::milliseconds(0));
		// if the state isn't loaded yet, draw loading screen and return
		if(status != std::future_status::ready) {
			m_loadingState->step();
			return;
		}
		else {
			m_loadingFuture.get();
			m_loadingState.reset();
			if(m_lastResize != sf::Vector2i(0, 0)) {
				m_stack.back()->handleWindowResize(m_lastResize);
				m_lastResize = sf::Vector2i(0, 0);
			}
		}
	}

	// update all the states from top to bottom until one of them returns false
	for(auto it = m_stack.rbegin(); it != m_stack.rend(); ++it) {
		if(!(*it)->step()) break;
	}

	applyPendingChanges();
}

void SceneStack::render() const {
	// a state is loading
	if(m_loadingFuture.valid()) {
		auto status = m_loadingFuture.wait_for(std::chrono::milliseconds(10));
		// if the state isn't loaded yet, draw loading screen and return
		if(status != std::future_status::ready) {
			m_loadingState->render();
			return;
		}
	}

	for(auto it = m_stack.rbegin(); it != m_stack.rend(); ++it) {
		if(!(*it)->render()) break;
	}
}

void SceneStack::handleIOEvent(sf::Event const& event) {
	// do not handle events while loading states
	if(m_loadingFuture.valid()) return;

	// from top to bottom until one state returns false
	for(auto it = m_stack.rbegin(); it != m_stack.rend(); ++it) {
		if(!(*it)->handleIOEvent(event)) break;
	}

	applyPendingChanges();
}

void SceneStack::pushScene(Scene::ID stateID) {
	m_pendingList.push_back(PendingChange(Action::Push, stateID));
}

void SceneStack::popScene() {
	m_pendingList.push_back(PendingChange(Action::Pop));
}

void SceneStack::clearScenes() {
	m_pendingList.push_back(PendingChange(Action::Clear));
}

bool SceneStack::isEmpty() const {
	return m_stack.empty();
}

void SceneStack::handleWindowResize(sf::Vector2i const& size) {
	if(m_loadingFuture.valid()) {
		m_loadingState->handleWindowResize(size);
		m_lastResize = size;
	}

	for(auto& child: m_stack)
		child->handleWindowResize(size);
}

Scene::Ptr SceneStack::createScene(Scene::ID stateID) {
	auto found = m_factories.find(stateID);
	assert(found != m_factories.end());

	return found->second();
}

void SceneStack::applyPendingChanges() {
	for(auto const& change: m_pendingList) {
		switch(change.action) {
			case Action::Push:
				if(m_loading.find(change.stateID) != m_loading.end()) {
					m_loadingState  = std::make_unique<Scene::Loading>(*this, m_context);
					m_loadingFuture = std::async(std::launch::async, [this, change]() {
						m_stack.push_back(createScene(change.stateID));
					});
				}
				else {
					if(!m_stack.empty()) m_stack.back()->onLeave();
					m_stack.push_back(createScene(change.stateID));
				}
				break;
			case Action::Pop:
				m_stack.pop_back();
				if(!m_stack.empty()) m_stack.back()->onEnter();
				break;
			case Action::Clear: m_stack.clear(); break;
		}
	}

	m_pendingList.clear();
}

SceneStack::PendingChange::PendingChange(Action action, Scene::ID stateID):
    action(action), stateID(stateID) {}

