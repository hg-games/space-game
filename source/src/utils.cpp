#include "utils.hpp"

#include <mutex>

/*
 * running average helper struct
 */
template<bool enable_minmax = true, size_t buf_size = 100, class Ratio = std::milli,
         class DiffType = std::chrono::duration<float, Ratio>>
class ravg {
public:
	using diff_type = DiffType;
	using time_type = typename DiffType::rep;
	using ratio     = Ratio;

	void start() { m_start = utils::time::now(); }

	void end() {
		m_end          = utils::time::now();
		diff_type diff = m_end - m_start;

		if constexpr(enable_minmax) {
			m_min = *std::min_element(m_buf.begin(), m_buf.end());
			m_max = *std::max_element(m_buf.begin(), m_buf.end());
		}

		m_buf[m_index] = diff;
		m_total += diff; // add new value
		// remove oldest value
		m_total -= m_buf[(m_index + 1) % m_buf.size()];
		m_avg = m_total / m_buf.size();

		m_index++;
		if(m_index >= m_buf.size()) {
			m_index = 0;
		}
	}

	std::array<time_type, 3> get_stats() const { return {get_avg(), get_min(), get_max()}; }

	time_type get_avg() const { return m_avg.count(); }

	time_type get_max() const { return m_max.count(); }

	time_type get_min() const { return m_min.count(); }

private:
	std::array<diff_type, buf_size> m_buf;
	size_t                          m_index {0};

	utils::time::time_point m_start;
	utils::time::time_point m_end;

	diff_type m_avg;
	diff_type m_max {std::numeric_limits<time_type>::lowest()};
	diff_type m_min {std::numeric_limits<time_type>::max()};
	diff_type m_total;
};

namespace {
std::mutex                                timers_mutex;
std::unordered_map<std::uint64_t, ravg<>> timers;
} // namespace

Timer::Scoped::Scoped(std::uint64_t id): m_id(id) {
	std::lock_guard<std::mutex> lk(timers_mutex);
	timers[id].start();
}

Timer::Scoped::~Scoped() {
	std::lock_guard<std::mutex> lk(timers_mutex);
	timers[m_id].end();
}

Timer::Manual::Manual(std::uint64_t id): m_id(id) {}

void Timer::Manual::start() {
	std::lock_guard<std::mutex> lk(timers_mutex);
	timers[m_id].start();
}

void Timer::Manual::stop() {
	std::lock_guard<std::mutex> lk(timers_mutex);
	timers[m_id].end();
}

float Timer::get_avg(std::uint64_t id) {
	std::lock_guard<std::mutex> lk(timers_mutex);
	return timers[id].get_avg();
}

std::array<float, 3> Timer::get_stats(std::uint64_t id) {
	std::lock_guard<std::mutex> lk(timers_mutex);
	return timers[id].get_stats();
}
