#pragma once

#include <cstdint>
#include <string>
#include <tuple>

#include "SFML/System/Vector2.hpp"

struct Orbit {
	Orbit() = default;

	std::pair<sf::Vector2<double>, sf::Vector2<double>> getStateVectors(double time = 0.f) const;
	void                                                setOrbitedBody(std::uint32_t name_hash);
	void setFromStateVectors(sf::Vector2<double> const& pos, sf::Vector2<double> const& vel);

	double period() const;

	// required parameters to define an orbit at an epoch
	float  eccentricity {0.f};
	float  arg_of_periapsis {0.f};
	float  mean_anomaly {0.f};
	double semi_major_axis {0.0};
	double epoch {0.0};

	bool                apply_shift {false};
	sf::Vector2<double> position_shift;
	sf::Vector2<double> velocity_shift;

	// state vector computed from orbital params
	sf::Vector2<double> position {};
	sf::Vector2<double> velocity {};

	// orbited body
	double        mu {0.f}; // cached value to prevent querying the orbited body every frame
	std::string   body;
	std::uint32_t body_name_hash {0};
};
