#include "PropertyTree.hpp"

#include <algorithm>

#include "spdlog/fmt/bundled/format.h"

PropertyTreeKeyException::PropertyTreeKeyException(std::string_view key) noexcept: m_key(key) {}

const char* PropertyTreeKeyException::what() const noexcept {
	return fmt::format("PropertyTree: key [{}] does not exist", m_key).data();
}

PropertyTree::PropertyTree(std::string_view expr) {
	if(expr.empty())
		throw std::runtime_error("Cannot create PropertyTree from empty expression string");

	const std::string_view::size_type end = expr.find('.');

	// first substr is the key for this three
	m_key = expr.substr(0, end);
	if(m_key.empty())
		throw std::runtime_error(
		    fmt::format("Cannot create PropertyTree from invalid expression string: {}", expr));

	// if there is still data after the key ("foo.bar")
	if(end != std::string_view::npos) {
		// create a child with the rest of the expression
		operator[](expr.substr(end + 1));
	}
}

PropertyTree& PropertyTree::operator[](std::string_view expr) {
	if(expr.empty()) return *this;

	std::string_view                  key;
	std::string_view                  path;
	const std::string_view::size_type end = expr.find('.');

	// it's a simple key
	if(end == std::string_view::npos) {
		key = expr;
	}
	else {
		// grab the key part and the path
		key  = expr.substr(0, end);
		path = expr.substr(end + 1);
	}

	const auto it = std::find_if(m_children.begin(), m_children.end(),
	                             [key](auto const& ptree) { return ptree.m_key == key; });

	if(it == m_children.end()) {
		return m_children.emplace_back(expr);
	}

	return it->operator[](path);
}

PropertyTree const& PropertyTree::operator[](std::string_view expr) const {
	if(expr.empty()) return *this;

	std::string_view                  key;
	std::string_view                  path;
	const std::string_view::size_type end = expr.find('.');

	// it's a simple key
	if(end == std::string_view::npos) {
		key = expr;
	}
	else {
		// grab the key part and the path
		key  = expr.substr(0, end);
		path = expr.substr(end + 1);
	}

	const auto it = std::find_if(m_children.begin(), m_children.end(),
	                             [key](auto const& ptree) { return ptree.m_key == key; });

	if(it == m_children.end()) {
		// return m_children.emplace_back(expr);
		// this cannot happen in a const context
		throw PropertyTreeKeyException(key);
	}

	return it->operator[](path);
}

void PropertyTree::writeLayoutToStr(std::string& out_str, int indent) const {
	if(indent > 0) out_str += '\n';

	for(int i = 0; i < indent; i++)
		out_str += "  ";

	out_str += m_key;
	if(!has_data())
		for(auto const& child: m_children)
			child.writeLayoutToStr(out_str, indent + 1);
	else {
		auto write_func = [&](auto&& arg) {
			out_str += ": ";

			auto write_data = [&](auto&& data) {
				using U = std::decay_t<decltype(data)>;
				if constexpr(std::is_same_v<U, std::string>)
					out_str += data;
				else if constexpr(std::is_same_v<U, double>)
					out_str += fmt::format("{}", data);
				else if constexpr(std::is_same_v<U, bool>)
					out_str += (data ? "true" : "false");
				else if constexpr(std::is_same_v<U, std::monostate>)
					out_str += "nil";
				else if constexpr(std::is_same_v<U, std::vector<Data>>)
					out_str += "wut";
			};

			using T = std::decay_t<decltype(arg)>;
			if constexpr(std::is_same_v<T, std::vector<Data>>) {
				out_str += "[";
				for(const auto& d: arg) {
					std::visit(write_data, d.raw);
					out_str += ", ";
				}
				out_str.erase(out_str.size() - 2);
				out_str += "]";
			}
			else {
				write_data(arg);
			}
		};
		std::visit(write_func, m_data.raw);
	}
}

bool PropertyTree::has(std::string_view expr) const {
	try {
		operator[](expr);
		return true;
	}
	catch(PropertyTreeKeyException const& e) {
		return false;
	}
}
