#pragma once

#include <string_view>

#include "SFML/System/Vector2.hpp"

namespace sf {
class Event;
class RenderWindow;
} // namespace sf

class InputHandler {
public:
	/*
	 * What in game action the controller should manage.
	 * Those are immediate controls meaning they have the
	 * same state as the keyboard/button bound to them.
	 * There is no pressed/released event
	 */
	enum class ImmediateControl : unsigned int {
		MOVE_UP = 0,
		MOVE_DOWN,
		MOVE_LEFT,
		MOVE_RIGHT,

		SHOOT,

		COUNT
	};

public:
	static void                       create();
	static constexpr std::string_view getControlName(ImmediateControl ctrl);

	void update(sf::RenderWindow& window);

	void handleEvent(sf::Event const& event);

	bool isControlActive(ImmediateControl ctrl) const;

	sf::Vector2f getMousePosition() const;

private:
	InputHandler();

	/*
	 * Read keyboard + mouse state
	 *
	 * This function just check if a key/button is pressed
	 * or not, it doesn't know if it was just pressed or released
	 */
	void updateImmediateInput();

private:
	bool         m_hasFocus;
	sf::Vector2f m_mousePos;
};

extern InputHandler* g_inputHandler;
