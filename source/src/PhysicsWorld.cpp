#include "PhysicsWorld.hpp"

#include "box2d/b2_body.h"

#include "Entities/details/Terrain.hpp"
#include "Logger.hpp"
#include "Planetarium.hpp"
#include "box2dutils.hpp"
#include "constants.hpp"

PhysicsWorld::PhysicsWorld(sf::Vector2<double> const& origin):
    m_world({0.f, 0.f}), m_origin(origin) {}

void PhysicsWorld::step() {
	m_world.Step(timestep, 8, 3);
	for(auto& vptr: m_vessels) {
		const auto vel = vptr->getBody()->GetLinearVelocity();
		if(vel != vptr->m_physicsVelocity) {
			const auto pos        = vptr->getBody()->GetPosition();
			const auto [dvx, dvy] = vel - vptr->m_physicsVelocity;
			const auto [dpx, dpy] = pos - vptr->m_physicsPosition;

			Orbit& orbit         = vptr->getOrbit();
			orbit.velocity_shift = {dvx, dvy};
			orbit.position_shift = {dpx, dpy};
			orbit.apply_shift    = true;

			vptr->getBody()->SetLinearVelocity({0, 0});
		}

		vptr->step();
		setOrigin(g_planetarium->getGlobalVesselPosition(vptr->id));
		vptr->m_physicsPosition.SetZero();
		vptr->m_physicsVelocity.SetZero();
		vptr->getBody()->SetTransform(b2Vec2(0, 0), vptr->getBody()->GetAngle());
	}
}

void PhysicsWorld::setTerrain(Terrain* terrain) {
	m_terrain = terrain;
}

void PhysicsWorld::registerVessel(std::shared_ptr<Vessel> vessel) {
	// transfer from old world
	if(vessel->hasPhysicsWorld()) {
		transferVesselFrom(vessel->getPhysicsWorld(), vessel);
	}
	else {
		vessel->setPhysicsWorld(this);
		vessel->createBody();
		vessel->m_physicsPosition = {};
		vessel->m_physicsVelocity = {};

		m_vessels.push_back(vessel);
	}
}

void PhysicsWorld::transferVesselFrom(PhysicsWorld* other, std::shared_ptr<Vessel> vessel) {
	auto old_body = vessel->getBody();
	auto body     = b2utils::clone(&m_world, old_body);

	// shift vessel pos to the new coords
	auto pos = other->getOrigin() - m_origin;
	pos.x += old_body->GetPosition().x;
	pos.y += old_body->GetPosition().y;

	body->SetTransform(b2Vec2(pos.x, pos.y), body->GetAngle());

	other->destroyVessel(vessel->id);
	vessel->setBody(body);
	vessel->setPhysicsWorld(this);
	vessel->m_physicsPosition = body->GetPosition();
	vessel->m_physicsVelocity = body->GetLinearVelocity();

	m_vessels.push_back(vessel);
}

std::shared_ptr<Vessel> PhysicsWorld::destroyVessel(std::uint64_t id) {
	for(auto it = m_vessels.begin(); it != m_vessels.end(); ++it) {
		if((*it)->id == id) {
			auto ptr = (*it);
			m_world.DestroyBody(ptr->getBody());
			m_vessels.erase(it);
			return ptr;
		}
	}

	return nullptr;
}

void PhysicsWorld::setOrigin(sf::Vector2<double> const& origin) {
	m_origin = origin;
}

sf::Vector2f PhysicsWorld::getLocalPosition(sf::Vector2<double> const& global_position) const {
	return sf::Vector2f(global_position - m_origin);
}

sf::Rect<std::int64_t> PhysicsWorld::getAABB() const {
	sf::Rect<std::int64_t> r;
	r.left   = std::floor(m_origin.x - physics::simSize / 2);
	r.top    = std::floor(m_origin.y - physics::simSize / 2);
	r.width  = physics::simSize;
	r.height = physics::simSize;

	return r;
}
