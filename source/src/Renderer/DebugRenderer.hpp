#pragma once

#include <vector>

#include "SFML/Graphics/Color.hpp"
#include "SFML/Graphics/VertexArray.hpp"
#include "SFML/System/Vector2.hpp"

class Camera;
namespace sf {
class RenderTarget;
}

class DebugRenderer {
public:
	static void create();

	void set_camera(const Camera* camera);

	// base draw functions
	void draw_line(sf::Vector2f const& from, sf::Vector2f const& to, sf::Color const& color);
	void draw_linestrip(std::vector<sf::Vector2f> const& points, sf::Color const& color);

	void draw_solid_polygon(std::vector<sf::Vector2f> const& vertices, sf::Color const& color);
	void draw_polygon(std::vector<sf::Vector2f> const& vertices, sf::Color const& color);

	// helper draw functions (call the base functions)
	void draw_quad(sf::Vector2f const& pos, sf::Vector2f const& size, sf::Color const& color);
	void draw_solid_quad(sf::Vector2f const& pos, sf::Vector2f const& size, sf::Color const& color);

	void draw_circle(sf::Vector2f const& center, float radius, sf::Color const& color);
	void draw_solid_circle(sf::Vector2f const& center, float radius, sf::Color const& color);

	void draw_ellipse(sf::Vector2f const& center, float a, float b, float angle, std::size_t count,
	                  sf::Color const& color);
	void draw_solid_ellipse(sf::Vector2f const& center, float a, float b, float angle,
	                        std::size_t count, sf::Color const& color);

	void draw_arrow(sf::Vector2f const& from, sf::Vector2f const& to, sf::Color const& color);
	void draw_point(sf::Vector2f const& pos, sf::Color const& color);

	void clear();
	void render(sf::RenderTarget& target) const;

private:
	DebugRenderer();

private:
	sf::VertexArray m_lines;
	sf::VertexArray m_triangles;

	const Camera* m_camera {nullptr};
};

extern DebugRenderer* g_debugRenderer;
