#pragma once

#include <cstdint>

class Terrain;
class Camera;

namespace sf {
class RenderTarget;
}

class TerrainRenderer {
public:
	TerrainRenderer();

	void setChunkCoord(std::int32_t x, std::int32_t y);
	void setTerrain(Terrain const& t);
	void setCamera(Camera const& c);

	// returns true is chunks are visible with the current chunk coords and current camera
	bool hasVisibleChunks() const;

	void render(sf::RenderTarget& target) const;

private:
	std::int32_t m_chunkx {0};
	std::int32_t m_chunky {0};

	const Terrain* m_terrain {nullptr};
	const Camera*  m_camera {nullptr};
};
