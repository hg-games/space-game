#include "DebugRenderer.hpp"

#include <cmath>

#include "SFML/Graphics/RenderTarget.hpp"

#include "Camera.hpp"
#include "DebugInfo.hpp"
#include "utils.hpp"

DebugRenderer* g_debugRenderer = nullptr;

void DebugRenderer::create() {
	if(g_debugRenderer == nullptr) g_debugRenderer = new DebugRenderer();
}

DebugRenderer::DebugRenderer(): m_lines(sf::Lines), m_triangles(sf::Triangles) {}

void DebugRenderer::set_camera(const Camera* camera) {
	m_camera = camera;
}

void DebugRenderer::draw_line(sf::Vector2f const& from, sf::Vector2f const& to,
                              sf::Color const& color) {
	m_lines.append(sf::Vertex {from, color});
	m_lines.append(sf::Vertex {to, color});
}

void DebugRenderer::draw_linestrip(std::vector<sf::Vector2f> const& points,
                                   sf::Color const&                 color) {
	if(points.empty()) return;

	for(size_t i = 1; i < points.size(); i++) {
		m_lines.append(sf::Vertex {points[i - 1], color});
		m_lines.append(sf::Vertex {points[i], color});
	}
}

void DebugRenderer::draw_solid_polygon(std::vector<sf::Vector2f> const& vertices,
                                       sf::Color const&                 color) {
	if(vertices.empty()) return;

	const sf::Vertex v0 {vertices.front(), color};

	for(size_t i = 2; i < vertices.size(); i++) {
		m_triangles.append(v0);
		m_triangles.append(sf::Vertex {vertices[i - 1], color});
		m_triangles.append(sf::Vertex {vertices[i], color});
	}
}

void DebugRenderer::draw_polygon(std::vector<sf::Vector2f> const& vertices,
                                 sf::Color const&                 color) {
	if(vertices.empty()) return;

	for(size_t i = 1; i < vertices.size(); i++) {
		m_lines.append(sf::Vertex {vertices[i - 1], color});
		m_lines.append(sf::Vertex {vertices[i], color});
	}

	m_lines.append(sf::Vertex {vertices.front(), color});
	m_lines.append(sf::Vertex {vertices.back(), color});
}

void DebugRenderer::draw_quad(sf::Vector2f const& pos, sf::Vector2f const& size,
                              sf::Color const& color) {
	const std::vector points {pos, {pos.x, pos.y + size.y}, pos + size, {pos.x + size.x, pos.y}};
	draw_polygon(points, color);
}

void DebugRenderer::draw_solid_quad(sf::Vector2f const& pos, sf::Vector2f const& size,
                                    sf::Color const& color) {
	const std::vector points {pos, {pos.x, pos.y + size.y}, pos + size, {pos.x + size.x, pos.y}};
	draw_solid_polygon(points, color);
}

void DebugRenderer::draw_circle(sf::Vector2f const& center, float radius, sf::Color const& color) {
	constexpr auto            pi    = 3.141592654f;
	constexpr auto            count = 32;
	std::vector<sf::Vector2f> points(count);

	for(int i = 0; i < count; i++) {
		auto& p     = points[i];
		float angle = i * 2 * pi / count - pi / 2;
		p.x         = std::cos(angle) * radius + center.x;
		p.y         = std::sin(angle) * radius + center.y;
	}

	draw_polygon(points, color);
}

void DebugRenderer::draw_solid_circle(sf::Vector2f const& center, float radius,
                                      sf::Color const& color) {
	constexpr auto            pi    = 3.141592654f;
	constexpr auto            count = 64;
	std::vector<sf::Vector2f> points(count);

	for(int i = 0; i < count; i++) {
		auto&       p     = points[i];
		const float angle = i * 2 * pi / count - pi / 2;
		p.x               = std::cos(angle) * radius + center.x;
		p.y               = std::sin(angle) * radius + center.y;
	}

	draw_solid_polygon(points, color);
}

void DebugRenderer::draw_ellipse(sf::Vector2f const& center, float a, float b, float angle,
                                 std::size_t count, sf::Color const& color) {
	constexpr auto            pi = 3.141592654f;
	std::vector<sf::Vector2f> points(count);

	angle *= utils::degToRad;

	for(std::size_t i = 0; i < count; i++) {
		auto&       p = points[i];
		const float t = i * 2 * pi / count - pi / 2;

		const float x = a * std::cos(t);
		const float y = b * std::sin(t);

		p.x = x * std::cos(angle) - y * std::sin(angle) + center.x;
		p.y = x * std::sin(angle) + y * std::cos(angle) + center.y;
	}

	draw_polygon(points, color);
	{
		const float  x = a * std::cos(0);
		const float  y = b * std::sin(0);
		sf::Vector2f p;

		p.x = x * std::cos(angle) - y * std::sin(angle) + center.x;
		p.y = x * std::sin(angle) + y * std::cos(angle) + center.y;

		draw_line(p, center, color);
	}
	{
		const float  x = a * std::cos(pi);
		const float  y = b * std::sin(pi);
		sf::Vector2f p;

		p.x = x * std::cos(angle) - y * std::sin(angle) + center.x;
		p.y = x * std::sin(angle) + y * std::cos(angle) + center.y;

		draw_line(p, center, color);
	}
}

void DebugRenderer::draw_solid_ellipse(sf::Vector2f const& center, float a, float b, float angle,
                                       std::size_t count, sf::Color const& color) {
	constexpr auto            pi = 3.141592654f;
	std::vector<sf::Vector2f> points(count);

	angle *= utils::degToRad;

	for(std::size_t i = 0; i < count; i++) {
		auto&       p = points[i];
		const float t = i * 2 * pi / count - pi / 2;

		const float x = a * std::cos(t);
		const float y = b * std::sin(t);

		p.x = x * std::cos(angle) - y * std::sin(angle) + center.x;
		p.y = x * std::sin(angle) + y * std::cos(angle) + center.y;
	}

	draw_solid_polygon(points, color);
}

void DebugRenderer::draw_arrow(sf::Vector2f const& from, sf::Vector2f const& to,
                               sf::Color const& color) {
	const float angle  = utils::angle(to - from);
	const float length = utils::length(to - from);

	draw_line(from, to, color);
	draw_line(to
	              - sf::Vector2f(std::cos(angle + 45 * utils::degToRad),
	                             std::sin(angle + 45 * utils::degToRad))
	                    * length / 4.f,
	          to, color);
	draw_line(to
	              - sf::Vector2f(std::cos(angle - 45 * utils::degToRad),
	                             std::sin(angle - 45 * utils::degToRad))
	                    * length / 4.f,
	          to, color);
}

void DebugRenderer::draw_point(sf::Vector2f const& pos, sf::Color const& color) {
	draw_line(pos + sf::Vector2f {-2, -2}, pos + sf::Vector2f {2, 2}, color);
	draw_line(pos + sf::Vector2f {-2, 2}, pos + sf::Vector2f {2, -2}, color);
}

void DebugRenderer::clear() {
	g_debugInfo->debug_vertex_count = m_lines.getVertexCount() + m_triangles.getVertexCount();
	m_lines.clear();
	m_triangles.clear();
}

void DebugRenderer::render(sf::RenderTarget& target) const {
	if(!m_camera) return;

	const auto v = target.getView();
	target.setView(*m_camera);

	target.draw(m_triangles);
	target.draw(m_lines);

	target.setView(v);
}
