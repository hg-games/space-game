#include "TerrainRenderer.hpp"

#include "SFML/Graphics/RenderTarget.hpp"

#include "Camera.hpp"
#include "DebugInfo.hpp"
#include "Entities/details/Terrain.hpp"
#include "constants.hpp"

TerrainRenderer::TerrainRenderer() {}

void TerrainRenderer::setChunkCoord(std::int32_t x, std::int32_t y) {
	m_chunkx = x;
	m_chunky = y;
}

void TerrainRenderer::setTerrain(Terrain const& t) {
	m_terrain = &t;
}

void TerrainRenderer::setCamera(Camera const& c) {
	m_camera = &c;
}

bool TerrainRenderer::hasVisibleChunks() const {
	if(!m_camera || !m_terrain) return false;

	const auto [w, h] = m_camera->getSize() * pixelToMeter;
	const auto [l, t] = (m_camera->getCenter() - m_camera->getSize() / 2.f) * pixelToMeter;

	std::int32_t fromx = m_chunkx;
	std::int32_t fromy = m_chunky;
	std::int32_t tox   = fromx;
	std::int32_t toy   = fromy;

	for(auto i = l; i < 0.f; i += Chunk::size)
		fromx--;
	for(auto i = t; i < 0.f; i += Chunk::size)
		fromy--;
	for(auto i = l + w; i > Chunk::size; i -= Chunk::size)
		tox++;
	for(auto i = t + h; i > Chunk::size; i -= Chunk::size)
		toy++;

	for(std::int32_t cx = fromx; cx <= tox; cx++) {
		for(std::int32_t cy = fromy; cy <= toy; cy++) {
			if(!m_terrain->hasChunk(cx, cy)) continue;
			return true;
		}
	}

	return false;
}

void TerrainRenderer::render(sf::RenderTarget& target) const {
	g_debugInfo->rendered_chunks = 0;

	if(!m_terrain || !m_camera) return;

	const auto [w, h] = m_camera->getSize() * pixelToMeter;
	const auto [l, t] = (m_camera->getCenter() - m_camera->getSize() / 2.f) * pixelToMeter;

	std::int32_t fromx = m_chunkx;
	std::int32_t fromy = m_chunky;
	std::int32_t tox   = fromx;
	std::int32_t toy   = fromy;

	for(auto i = l; i < 0.f; i += Chunk::size)
		fromx--;
	for(auto i = t; i < 0.f; i += Chunk::size)
		fromy--;
	for(auto i = l + w; i > Chunk::size; i -= Chunk::size)
		tox++;
	for(auto i = t + h; i > Chunk::size; i -= Chunk::size)
		toy++;

	for(std::int32_t cx = fromx; cx <= tox; cx++) {
		for(std::int32_t cy = fromy; cy <= toy; cy++) {
			if(!m_terrain->hasChunk(cx, cy)) continue;

			g_debugInfo->rendered_chunks++;

			sf::RenderStates states;
			if(cx != m_chunkx || cy != m_chunky) {
				states.transform.translate((cx - m_chunkx) * Chunk::size * meterToPixel,
				                           (cy - m_chunky) * Chunk::size * meterToPixel);
			}
			sf::Vertex v[5];
			v[0].position = {};

			v[1].position.x = Chunk::size * meterToPixel;
			v[1].position.y = 0;

			v[2].position.x = Chunk::size * meterToPixel;
			v[2].position.y = Chunk::size * meterToPixel;

			v[3].position.x = 0;
			v[3].position.y = Chunk::size * meterToPixel;

			v[4] = v[0];

			v[0].color = v[1].color = v[2].color = v[3].color = v[4].color = sf::Color::Green;

			target.draw(m_terrain->m_chunks.at({cx, cy}).varray, states);
			target.draw(v, 5, sf::LineStrip, states);
		}
	}
}
