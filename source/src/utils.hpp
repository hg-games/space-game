#ifndef DEF_UTILS_HPP
#define DEF_UTILS_HPP

#include <chrono>
#include <cmath>      // atan2, acos
#include <functional> // hash

namespace utils {
namespace time {
	using time_point = std::chrono::time_point<std::chrono::high_resolution_clock>;
	inline auto now() { return std::chrono::high_resolution_clock::now(); }
} // namespace time

template<class Vector>
constexpr float angle(Vector const& vec) {
	return std::atan2(vec.y, vec.x);
}

template<class Vector>
Vector& normalize(Vector& vec) {
	const auto length = std::sqrt(vec.x * vec.x + vec.y * vec.y);
	vec /= length;
	return vec;
}

template<class Vector>
Vector normalized(Vector const& vec) {
	const auto length = std::sqrt(vec.x * vec.x + vec.y * vec.y);
	return vec / length;
}

template<class Vector>
auto length(Vector const& vec) {
	return std::sqrt(vec.x * vec.x + vec.y * vec.y);
}

static constexpr double radToDeg {180.0 / 3.141592653589793};
static constexpr double degToRad {1.0 / radToDeg};

} // namespace utils

static constexpr void hash_combine(std::size_t& h, std::size_t const& v) {
	h ^= v + 0x9e3779b9 + (h << 6) + (h >> 2);
}

/*
 * Hash a pair<T, T>
 */
struct pair_hash {
	template<typename T, typename U>
	constexpr std::size_t operator()(std::pair<T, U> const& p) const {
		std::size_t h = std::hash<T>()(p.first);
		hash_combine(h, std::hash<U>()(p.second));
		return h;
	}
};

/*
 * Hash a vector with member variable x,y
 */
struct vec2_hash {
	template<class Vector>
	constexpr std::size_t operator()(Vector const& v) const {
		std::size_t h = std::hash<decltype(v.x)>()(v.x);
		hash_combine(h, std::hash<decltype(v.y)>()(v.y));
		return h;
	}
};

struct Timer {
	struct Scoped {
		Scoped(std::uint64_t id);
		~Scoped();

		std::uint64_t m_id;
	};

	struct Manual {
		Manual(std::uint64_t id);

		void start();
		void stop();

		std::uint64_t m_id;
	};

	static float                get_avg(std::uint64_t id);
	static std::array<float, 3> get_stats(std::uint64_t id);
};
#endif
