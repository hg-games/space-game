#pragma once

#include "SFML/Graphics/View.hpp"

class Camera: public sf::View {
public:
	Camera() { setCenter(0, 0); }

	void zoom(float value) {
		sf::View::zoom(value);
		m_zoom *= value;
	}

	float getZoom() const { return m_zoom; }

	void setSize(float x, float y) {
		sf::View::setSize(x, y);
		m_zoom = 1.f;
	}

private:
	float m_zoom {1.f};
};
