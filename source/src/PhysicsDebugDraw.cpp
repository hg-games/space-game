#include "PhysicsDebugDraw.hpp"

#include "SFML/Graphics/RenderTarget.hpp"

#include "Renderer/DebugRenderer.hpp"

#include "constants.hpp"

PhysicsDebugDraw::PhysicsDebugDraw() {}

static sf::Color c(b2Color const& color) {
	return sf::Color(color.r * 255, color.g * 255, color.b * 255, 127);
}

void PhysicsDebugDraw::DrawPolygon(const b2Vec2* vertices, int32 vertexCount,
                                   const b2Color& color) {
	std::vector<sf::Vector2f> points(vertexCount);
	for(size_t i = 0; i < points.size(); i++)
		points[i] = sf::Vector2f {vertices[i].x * meterToPixel, vertices[i].y * meterToPixel};
	g_debugRenderer->draw_polygon(points, c(color));
}

void PhysicsDebugDraw::DrawSolidPolygon(const b2Vec2* vertices, int32 vertexCount,
                                        const b2Color& color) {
	std::vector<sf::Vector2f> points(vertexCount);
	for(size_t i = 0; i < points.size(); i++)
		points[i] = sf::Vector2f {vertices[i].x * meterToPixel, vertices[i].y * meterToPixel};
	g_debugRenderer->draw_solid_polygon(points, c(color));
}

void PhysicsDebugDraw::DrawCircle(const b2Vec2& center, float radius, const b2Color& color) {
	g_debugRenderer->draw_circle(sf::Vector2f {center.x * meterToPixel, center.y * meterToPixel},
	                             radius * meterToPixel, c(color));
}

void PhysicsDebugDraw::DrawSolidCircle(const b2Vec2& center, float radius, const b2Vec2& axis,
                                       const b2Color& color) {
	g_debugRenderer->draw_solid_circle(
	    sf::Vector2f {center.x * meterToPixel, center.y * meterToPixel}, radius * meterToPixel,
	    c(color));
	g_debugRenderer->draw_line(sf::Vector2f {center.x, center.y} * meterToPixel,
	                           sf::Vector2f {center.x + axis.x, center.y + axis.y} * meterToPixel,
	                           c(color));
}

void PhysicsDebugDraw::DrawSegment(const b2Vec2& p1, const b2Vec2& p2, const b2Color& color) {
	g_debugRenderer->draw_line(sf::Vector2f {p1.x, p1.y} * meterToPixel,
	                           sf::Vector2f {p2.x, p2.y} * meterToPixel, c(color));
}

void PhysicsDebugDraw::DrawTransform([[maybe_unused]] const b2Transform& xf) {}

void PhysicsDebugDraw::DrawPoint(const b2Vec2& p, [[maybe_unused]] float size,
                                 const b2Color& color) {
	g_debugRenderer->draw_point(sf::Vector2f {p.x, p.y} * meterToPixel, c(color));
}
