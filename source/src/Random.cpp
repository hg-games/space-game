#include "Random.hpp"

#include <algorithm>
#include <iterator>

namespace {
thread_local std::mt19937_64 gen;
thread_local std::uint64_t   _seed {0};
} // namespace

void Random::seed(std::uint64_t seed) {
	_seed = seed;
	gen.seed(seed);
}

std::uint64_t Random::getSeed() {
	return _seed;
}

std::mt19937_64 const& Random::generator() {
	return gen;
}

std::uint64_t Random::integer() {
	return gen();
}

double Random::real() {
	return static_cast<double>(gen()) / std::mt19937_64::max();
}

std::string Random::string(std::uint32_t length, std::string_view charset) {
	std::string str(length, 0);

	std::generate_n(std::back_inserter(str), length,
	                [&] { return charset[gen() % charset.size()]; });

	return str;
}
