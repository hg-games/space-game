#include "box2dutils.hpp"

#include "box2d/b2_body.h"
#include "box2d/b2_fixture.h"
#include "box2d/b2_world.h"

namespace b2utils {
b2Body* clone(b2World* world, b2Body* body) {
	const auto bdef = bodyDef(body);
	auto       b    = world->CreateBody(&bdef);

	for(auto f = body->GetFixtureList(); f != nullptr; f = f->GetNext()) {
		clone(b, f);
	}

	return b;
}

b2Fixture* clone(b2Body* target, b2Fixture* source) {
	const auto fdef = fixtureDef(source);
	return target->CreateFixture(&fdef);
}

b2BodyDef bodyDef(b2Body* body) {
	b2BodyDef bdef;
	bdef.userData        = body->GetUserData();
	bdef.position        = body->GetPosition();
	bdef.angle           = body->GetAngle();
	bdef.linearVelocity  = body->GetLinearVelocity();
	bdef.angularVelocity = body->GetAngularVelocity();
	bdef.linearDamping   = body->GetLinearDamping();
	bdef.angularDamping  = body->GetAngularDamping();
	bdef.allowSleep      = body->IsSleepingAllowed();
	bdef.awake           = body->IsAwake();
	bdef.fixedRotation   = body->IsFixedRotation();
	bdef.bullet          = body->IsBullet();
	bdef.type            = body->GetType();
	bdef.enabled         = body->IsEnabled();
	bdef.gravityScale    = body->GetGravityScale();

	return bdef;
}

b2FixtureDef fixtureDef(b2Fixture* fixture) {
	b2FixtureDef fdef;
	fdef.shape       = fixture->GetShape();
	fdef.userData    = fixture->GetUserData();
	fdef.friction    = fixture->GetFriction();
	fdef.restitution = fixture->GetRestitution();
	fdef.density     = fixture->GetDensity();
	fdef.isSensor    = fixture->IsSensor();

	return fdef;
}

} // namespace b2utils
