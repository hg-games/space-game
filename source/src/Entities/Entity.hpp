#pragma once

#include <string>

struct Entity {
	enum class Type {
		Entity,
		Planet,
		Asteroid,
		Vessel,
	};

	Entity(Type t): type(t), id(next_id++) {}

	const Type type {Type::Entity};

	std::string   name;
	std::uint32_t name_hash;

	const std::uint64_t id;

private:
	static std::uint64_t next_id;
};

