#pragma once

#include <unordered_map>

#include "utils.hpp"

#include "Chunk.hpp"

class Terrain {
public:
	struct GeneratorParams {
		std::size_t seed;
		int         base_radius; // in meters
		int         amplitude;   // in meters
	};

public:
	// TODO: use some kind of noise expression specification to generate custom terrain
	void generate(GeneratorParams const& params);
	bool hasChunk(std::int32_t x, std::int32_t y) const;

	static sf::Vector2<std::int32_t> getChunkCoordsFromPosition(sf::Vector2<double> const& pos);

private:
	void generateSurface(GeneratorParams const& params);

private:
	std::unordered_map<std::pair<std::int32_t, std::int32_t>, Chunk, pair_hash> m_chunks;

	friend class TerrainRenderer;
};
