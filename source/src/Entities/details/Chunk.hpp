#pragma once

#include <cstdint>
#include <optional>

#include "box2d/b2_chain_shape.h"

#include "SFML/Graphics/VertexArray.hpp"

struct Chunk {
	struct Geometry {
		std::vector<sf::Vector2f> points;
		std::vector<std::int32_t> index;
		b2ChainShape              shape;
	};

	static constexpr std::int32_t size {1000}; // size of a chunk in meters

	// points within this distance of the chunk will also be added to this chunk
	static constexpr std::int32_t overlap {1};

	std::int32_t x, y; // chunk coords in [size]
	bool         solid {false};

	std::optional<Geometry> geometry;

	// TODO: remove
	sf::VertexArray varray {sf::LineStrip};
};
