#include "Terrain.hpp"

#include "Logger.hpp"
#include "Orbit.hpp"
#include "constants.hpp"
#include "hashed_string.hpp"
#include "utils.hpp"

constexpr float gradient[16] = {0.456988,  0.882373, 0.808681, 0.456312, 0.577436, 0.517888,
                                0.866556,  0.166618, 0.666918, 0.945806, 0.601387, 0.730279,
                                0.0871326, 0.24091,  0.587337, 0.354723};

static float smoothstep(float edge0, float edge1, float x) {
	x = std::clamp((x - edge0) / (edge1 - edge0), 0.f, 1.f);
	return x * x * (3 - 2 * x);
}

static float lerp(float t, float a, float b) {
	return a + t * (b - a);
}

void Terrain::generate(GeneratorParams const& params) {
	generateSurface(params);
}

bool Terrain::hasChunk(std::int32_t x, std::int32_t y) const {
	return m_chunks.count({x, y}) > 0;
}

sf::Vector2<std::int32_t> Terrain::getChunkCoordsFromPosition(sf::Vector2<double> const& pos) {
	return sf::Vector2<int32_t>(std::floor(pos.x / Chunk::size), std::floor(pos.y / Chunk::size));
}

void Terrain::generateSurface(GeneratorParams const& params) {
	g_logger->debug("Terrain: generating surface ...");
	Timer::Manual timer("terrain_surf_gen"_hs);
	timer.start();

	struct SurfacePoint {
		sf::Vector2f        pos;
		std::pair<int, int> cpos; // chunk pos
		SurfacePoint*       prev {nullptr};
		SurfacePoint*       next {nullptr};
	};
	const int point_count = static_cast<int>(2 * M_PI * params.base_radius);

	g_logger->debug("Terrain: generating {} surface points", point_count);
	g_logger->debug("Terrain: vector of surface points is {} KiB",
	                point_count * sizeof(SurfacePoint) / 1024);

	std::vector<SurfacePoint> points(point_count);
	points.front().prev = &points.back();
	points.back().next  = &points.front();

	const auto get_coords = [&params](int v) {
		return sf::Vector2<double>(
		    std::cos(static_cast<double>(v) / params.base_radius) * params.base_radius,

		    std::sin(static_cast<double>(v) / params.base_radius) * params.base_radius);
	};

	const auto set_point = [](auto& p, auto* prev, auto pos, auto cpos) {
		p.prev     = prev;
		prev->next = &p;
		p.pos      = pos;
		p.cpos     = cpos;
	};

	const auto add_point = [this](float x, float y, int cx, int cy) {
		if(!hasChunk(cx, cy)) {
			auto& chunk = m_chunks[{cx, cy}];
			chunk.geometry.emplace();
			chunk.geometry->points.emplace_back(x, y);
			chunk.geometry->index.emplace_back(0);
		}
		else {
			auto& chunk = m_chunks[{cx, cy}];
			chunk.geometry->points.emplace_back(x, y);
			for(auto& i: chunk.geometry->index) {
				i++;
			}
			chunk.geometry->index.emplace_back(0);
		}
	};

	// loop around the whole surface, put vertex every 1 meters
	for(int v = 1; v < point_count; v++) {
		const auto [fx, fy] = get_coords(v);
		const int chunkx    = std::floor(fx / Chunk::size);
		const int chunky    = std::floor(fy / Chunk::size);

		auto& chunk = m_chunks[{chunkx, chunky}];
		// surface point coords relative to chunk coords
		const float sx = fx - chunkx * Chunk::size;
		const float sy = fy - chunky * Chunk::size;
		// chunk.varray.append(sf::Vertex(sf::Vector2f(sx, sy) * meterToPixel, sf::Color::White));
		chunk.geometry->points.emplace_back(sx, sy);

		set_point(points[v], &points[v - 1], sf::Vector2f {sx, sy},
		          std::pair<int, int> {chunkx, chunky});

		if(sx < Chunk::overlap) {
			if(sy < Chunk::overlap) {
				add_point(sx + Chunk::size, sy + Chunk::size, chunkx - 1, chunky - 1);
			}
			else if(sy > Chunk::size - Chunk::overlap) {
				add_point(sx + Chunk::size, sy - Chunk::size, chunkx - 1, chunky + 1);
			}
			else {
				add_point(sx + Chunk::size, sy, chunkx - 1, chunky);
			}
		}
		else if(sx > Chunk::size - Chunk::overlap) {
			if(sy < Chunk::overlap) {
				add_point(sx - Chunk::size, sy + Chunk::size, chunkx + 1, chunky - 1);
			}
			else if(sy > Chunk::size - Chunk::overlap) {
				add_point(sx - Chunk::size, sy - Chunk::size, chunkx + 1, chunky + 1);
			}
			else {
				add_point(sx - Chunk::size, sy, chunkx + 1, chunky);
			}
		}
		else {
			if(sy < Chunk::overlap) {
				add_point(sx, sy + Chunk::size, chunkx, chunky - 1);
			}
			else if(sy > Chunk::size - Chunk::overlap) {
				add_point(sx, sy - Chunk::size, chunkx, chunky + 1);
			}
		}
	}
	// build vertex arrays and b2Shapes
	for(auto& [coords, chunk]: m_chunks) {
		const auto psize = chunk.geometry->points.size();
		if(psize < 2) {
			g_logger->warn("Terrain::generateSurface: chunk with {} surface point", psize);
			continue;
		}
		b2Vec2  b2vec[psize];
		b2Vec2* b2p = b2vec;
		for(auto const& p: chunk.geometry->points) {
			chunk.varray.append(sf::Vertex(p * meterToPixel, sf::Color::White));
			b2p->x = p.x;
			b2p->y = p.y;
			b2p++;
		}
		chunk.geometry->shape.CreateChain(b2vec, psize);
	}
	timer.stop();

	g_logger->debug("Terrain: surface generated in {:.1f}ms",
	                Timer::get_avg("terrain_surf_gen"_hs));
}
