#pragma once

#include "Entities/Astrobody.hpp"
#include "Entities/details/Terrain.hpp"

struct Planet: public Astrobody {
	Planet(): Astrobody(Entity::Type::Planet) {}

	Terrain terrain;
};
