#include "Asteroid.hpp"

void Asteroid::generate() {
	Terrain::GeneratorParams gp {};
	gp.seed        = seed;
	gp.base_radius = 1500;
	gp.amplitude   = 100;

	terrain.generate(gp);
}
