#pragma once

#include "Entities/Astrobody.hpp"
#include "Entities/details/Terrain.hpp"

/*
 * Asteroid surface do not get generated until needed
 * (vessel close, surface scan, preview, ...)
 */
struct Asteroid: public Astrobody {
	Asteroid(): Astrobody(Entity::Type::Asteroid) {}

	void generate();

	std::size_t seed {0};
	bool        generated {false};

	Terrain terrain;
};
