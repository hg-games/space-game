#pragma once

#include <optional>

#include "SFML/Graphics/Color.hpp"

#include "Entity.hpp"
#include "Orbit.hpp"

struct Astrobody: public Entity {
	Astrobody(Entity::Type t): Entity(t) {}

	double               mu;
	double               radius;
	std::optional<Orbit> orbit {std::nullopt};
	sf::Color            color {200, 200, 200};
};
