#pragma once

#include <memory>
#include <vector>

#include "Entity.hpp"
#include "Orbit.hpp"
#include "Parts/Base.hpp"

#include "box2d/b2_math.h"

struct b2Body;
struct b2World;
class PhysicsWorld;

class Vessel final: public Entity, public std::enable_shared_from_this<Vessel> {
public:
	Vessel();

	void step();

	std::size_t getPartCount() const { return m_parts.size(); }
	void        setRootPart(std::shared_ptr<Part::Base> part);
	bool        attachPart(std::shared_ptr<Part::Base> part);
	bool        surfaceAttachPart(std::shared_ptr<Part::Base> part);

	std::vector<std::shared_ptr<Part::Base>> const& getParts() const { return m_parts; }
	std::shared_ptr<Part::Base>                     getRootPart() const {
        return m_parts.empty() ? nullptr : m_parts.front();
	}

	bool         hasOrbit() const { return m_orbit.has_value(); }
	Orbit const& getOrbit() const { return m_orbit.value(); }
	Orbit&       getOrbit() { return m_orbit.value(); }
	void         setOrbit(Orbit const& orbit) { m_orbit.emplace(orbit); }

	bool          hasBody() const { return m_body != nullptr; }
	bool          hasPhysicsWorld() const { return m_physicsWorld != nullptr; }
	PhysicsWorld* getPhysicsWorld() const { return m_physicsWorld; }
	b2Body*       getBody() const { return m_body; }
	void          setBody(b2Body* body) { m_body = body; }
	void          setPhysicsWorld(PhysicsWorld* world);
	void          createBody();
	void          buildBodyFromParts();

private:
	std::optional<Orbit> m_orbit;

	std::vector<std::shared_ptr<Part::Base>> m_parts;

	// physics stuff
	b2Body*       m_body {nullptr};
	PhysicsWorld* m_physicsWorld {nullptr};
	b2Vec2        m_physicsPosition {0, 0};
	b2Vec2        m_physicsVelocity {0, 0}; // velocity inside the physics world

	friend class PhysicsWorld;
};
