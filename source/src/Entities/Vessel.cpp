#include "Vessel.hpp"

#include "box2d/b2_body.h"
#include "box2d/b2_circle_shape.h"
#include "box2d/b2_fixture.h"

#include "Logger.hpp"
#include "PhysicsWorld.hpp"
#include "Planetarium.hpp"

Vessel::Vessel(): Entity(Type::Vessel) {}

void Vessel::step() {}

void Vessel::setRootPart(std::shared_ptr<Part::Base> part) {
	if(part->mount_points) {
		for(auto& mp: *part->mount_points) {
			mp.self = part;
		}
	}
	m_parts.clear();
	m_parts.emplace_back(part);
}

bool Vessel::attachPart(std::shared_ptr<Part::Base> part) {
	if(part->mount_points) {
		for(auto& mp: *part->mount_points) {
			mp.self = part;
		}
	}
	m_parts.emplace_back(part);
	return true;
}

void Vessel::setPhysicsWorld(PhysicsWorld* world) {
	m_physicsWorld = world;
}

void Vessel::createBody() {
	buildBodyFromParts();
}

void Vessel::buildBodyFromParts() {
	if(!m_physicsWorld) {
		g_logger->warn("Vessel: trying to build a body with no assigned physics world");
		return;
	}

	b2BodyDef bdef;
	bdef.type = b2_dynamicBody;

	if(hasOrbit()) {
		const auto pos =
		    m_physicsWorld->getLocalPosition(g_planetarium->getGlobalVesselPosition(id));
		bdef.position = b2Vec2(pos.x, pos.y);
	}

	m_body = m_physicsWorld->getb2World()->CreateBody(&bdef);

	b2CircleShape shape;
	shape.m_radius = 100;

	b2FixtureDef fdef;
	fdef.density = 5;
	fdef.shape   = &shape;

	m_body->CreateFixture(&fdef);
}
