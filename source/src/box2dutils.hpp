#pragma once

struct b2Body;
struct b2BodyDef;
struct b2World;
struct b2Fixture;
struct b2FixtureDef;

namespace b2utils {
// clone an existing b2Body into a new body into world
b2Body* clone(b2World* world, b2Body* body);

// clone an existing b2Fixture into the target body
b2Fixture* clone(b2Body* target, b2Fixture* source);

// create a bodydef from an existing body
b2BodyDef bodyDef(b2Body* body);

// create a fixturedef from an existing fixture
b2FixtureDef fixtureDef(b2Fixture* fixture);

} // namespace b2utils
