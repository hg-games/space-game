#pragma once

#include <string_view>
#include <unordered_map>

namespace sf {
class Texture;
class Font;
} // namespace sf

class TextureAtlas;

class ResourceHandler {
public:
	static void create();

	void            loadDefaultFont(std::string_view path);
	sf::Font const& getDefaultFont() const { return *m_defaultFont; }

	/*
	 * For textures that are not part of an atlas
	 */
	void         loadTexture(size_t id, std::string_view path);
	sf::Texture* getTexture(size_t id);

	/*
	 * For texture atlases
	 */
	TextureAtlas* createTextureAtlas(size_t id);
	TextureAtlas* getTextureAtlas(size_t id);

private:
	ResourceHandler();

private:
	std::unordered_map<size_t, sf::Texture*>  m_textures;
	std::unordered_map<size_t, TextureAtlas*> m_atlases;

	sf::Font* m_defaultFont;
};

extern ResourceHandler* g_resources;
