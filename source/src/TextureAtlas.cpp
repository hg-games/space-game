#include "TextureAtlas.hpp"

#include "stb_rect_pack.h"

TextureAtlas::TextureAtlas(unsigned int width, unsigned int height):
    m_width(width), m_height(height) {}

bool TextureAtlas::loadTexture(std::size_t id, std::string_view path) {
	sf::Texture t;
	if(!t.loadFromFile(path.data())) return false;
	return m_textures.emplace(id, std::move(t)).second;
}

bool TextureAtlas::pack() {
	std::vector<stbrp_rect> rects;
	std::vector<stbrp_node> nodes;
	rects.resize(m_textures.size());
	nodes.resize(m_textures.size());

	int i = 0;
	for(auto const& [id, t]: m_textures) {
		rects[i] = stbrp_rect {i,
		                       static_cast<stbrp_coord>(t.getSize().x + 2),
		                       static_cast<stbrp_coord>(t.getSize().y + 2),
		                       0,
		                       0,
		                       0};
		i++;
	}
	stbrp_context context;
	stbrp_init_target(&context, m_width, m_height, nodes.data(), nodes.size());
	bool ret = !!stbrp_pack_rects(&context, rects.data(), rects.size());

	if(ret) {
		m_rects.clear();
		m_atlas.create(m_width, m_height);
		i = 0;
		for(auto const& [id, t]: m_textures) {
			m_atlas.update(t, rects[i].x + 1, rects[i].y + 1);
			m_rects.try_emplace(id, rects[i].x + 1, rects[i].y + 1, rects[i].w - 2, rects[i].h - 2);
			i++;
		}
		m_textures.clear();
	}

	return ret;
}

std::pair<sf::Texture const&, sf::IntRect const&> TextureAtlas::getTexture(std::size_t id) const {
	return {m_atlas, m_rects.at(id)};
}

