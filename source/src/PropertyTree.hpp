#pragma once

#include <exception>
#include <string>
#include <variant>
#include <vector>

class PropertyTreeKeyException final: public std::exception {
public:
	PropertyTreeKeyException(std::string_view key) noexcept;
	~PropertyTreeKeyException() = default;

	const char* what() const noexcept;

	std::string_view key() const noexcept { return m_key; }

private:
	std::string m_key;
};

class PropertyTree {
public:
	struct Data {
		std::variant<std::monostate, std::string, std::vector<Data>, double, bool> raw;

		Data() = default;
		Data(decltype(raw) const& d): raw(d) {}

		std::string&       as_string() { return std::get<std::string>(raw); }
		std::string const& as_string() const { return std::get<std::string>(raw); }

		bool& as_bool() { return std::get<bool>(raw); }
		bool  as_bool() const { return std::get<bool>(raw); }

		double& as_number() { return std::get<double>(raw); }
		double  as_number() const { return std::get<double>(raw); }

		std::vector<Data>&       as_vector() { return std::get<std::vector<Data>>(raw); }
		std::vector<Data> const& as_vector() const { return std::get<std::vector<Data>>(raw); }

		template<class T>
		std::pair<T, T> as_pair() const {
			auto const& v = as_vector();
			return {std::get<T>(v.at(0).raw), std::get<T>(v.at(1).raw)};
		}
		template<class T, class U>
		std::pair<T, U> as_pair() const {
			auto const& v = as_vector();
			return {std::get<T>(v.at(0).raw), std::get<U>(v.at(1).raw)};
		}
		std::pair<Data, Data> as_pair() const {
			auto const& v = as_vector();
			return {v.at(0), v.at(1)};
		}

		bool is_string() const { return std::holds_alternative<std::string>(raw); }
		bool is_bool() const { return std::holds_alternative<bool>(raw); }
		bool is_vector() const { return std::holds_alternative<std::vector<Data>>(raw); }
		bool is_number() const { return std::holds_alternative<double>(raw); }
		bool is_pair() const { return is_vector() && as_vector().size() == 2; }
	};

	PropertyTree(std::string_view expr);

	template<class T>
	void setData(T const& data) {
		m_data.raw.emplace<T>(data);
	}

	Data const& getData() const { return m_data; }
	Data&       getData() { return m_data; }
	bool        has(std::string_view expr) const;
	bool        has_data() const { return m_data.raw.index() != 0; }

	std::string_view key() const { return m_key; }

	PropertyTree&       operator[](std::string_view expr);
	PropertyTree const& operator[](std::string_view expr) const;

	void writeLayoutToStr(std::string& out_str, int indent = 0) const;

	auto begin() { return m_children.begin(); }
	auto end() { return m_children.end(); }

	auto begin() const { return m_children.begin(); }
	auto end() const { return m_children.end(); }

private:
	std::vector<PropertyTree> m_children;
	std::string               m_key;

	Data m_data {};
};
