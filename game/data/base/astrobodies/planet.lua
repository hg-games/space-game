data:extend(
{
	{
		type = "planet",
		name = "sun",

		radius = 695700000,
		mu = 1.32712440018e20,

		color = {255, 255, 70},
	},
	{
		type = "planet",
		name = "mercury",

		radius = 2439700,
		mass = 3.3022E+23,

		orbit = {
			eccentricity = 0.2056187266319207,
			semi_major_axis = 57908973645.88802,
			arg_of_periapsis = 66.90371044151551,
			mean_anomaly = 318.2162077814089,
			body = "sun",
		},

		color = {255, 255, 255},
	},
	{
		type = "planet",
		name = "venus",

		radius = 6049000,
		mass = 4.8676E+24,

		orbit = {
			eccentricity = 0.006810339650842032,
			semi_major_axis = 108209548790.4671,
			arg_of_periapsis = 123.7121294282329,
			mean_anomaly = 311.2459947553124,
			body = "sun",
		},

		color = {255, 255, 255},
	},
	{
		type = "planet",
		name = "earth",

		radius = 6371000,
		mu = 3.986004418e14,

		orbit = {
			eccentricity = 0.01609636160505683,
			semi_major_axis = 149598261150.4425,
			arg_of_periapsis = 102.9720683296131,
			mean_anomaly = 357.0607464120944,
			body = "sun",
		},

		color = {255, 255, 255},
	},
	{
		type = "planet",
		name = "moon",

		radius = 1737400,
		mass = 7.342e22,

		orbit = {
			eccentricity = 0.0549,
			semi_major_axis = 384399000,
			arg_of_periapsis = 0,
			mean_anomaly = 0,
			body = "earth",
		},

		color = {255, 255, 255},
	},
	{
		type = "planet",
		name = "mars",

		radius = 3375800,
		mu = 4.282831E+13,

		orbit = {
			eccentricity = 0.09326110278323557,
			semi_major_axis = 227949699961.9763,
			arg_of_periapsis = 332.1022655295414,
			mean_anomaly = 169.3913127942378,
			body = "sun",
		},

		color = {255, 255, 255},
	},
	{
		type = "planet",
		name = "phobos",

		radius = 7250,
		mass = 1.072E+16,

		orbit = {
			eccentricity = 0.01539938155583979,
			semi_major_axis = 9378492.209088314,
			arg_of_periapsis = 357.7759243021914,
			mean_anomaly = 7.185120835598890,
			body = "mars",
		},

		color = {255, 255, 255},
	},
	{
		type = "planet",
		name = "deimos",

		radius = 5456,
		mass = 1.48E+15,

		orbit = {
			eccentricity = 0.0003294680798661700,
			semi_major_axis = 23458112.01759387,
			arg_of_periapsis = 263.8963868784089,
			mean_anomaly = 323.5040336489673,
			body = "mars",
		},

		color = {255, 255, 255},
	},
	{
		type = "planet",
		name = "ceres",

		radius = 473000,
		mass = 9.39e+20,

		orbit = {
			eccentricity = 0.079363494880566,
			semi_major_axis = 413738762313.173,
			arg_of_periapsis = 129.19102663711,
			mean_anomaly = 60.1624710451615,
			body = "sun",
		},

		color = {255, 255, 255},
	},
	{
		type = "planet",
		name = "vesta",

		radius = 262700,
		mass = 2.59e+20,

		orbit = {
			eccentricity = 0.0902068412255369,
			semi_major_axis = 353346223803.158,
			arg_of_periapsis = 236.445369158826,
			mean_anomaly = 61.0607001442198,
			body = "sun",
		},

		color = {255, 255, 255},
	},
	{
		type = "planet",
		name = "jupiter",

		radius = 69373000,
		mu = 1.26686534e17,

		orbit = {
			eccentricity = 0.04872660654702194,
			semi_major_axis = 778188938659.7554,
			arg_of_periapsis = 10.75642751202877,
			mean_anomaly = 302.5812396096649,
			body = "sun",
		},

		color = {255, 255, 255},
	},
	{
		type = "planet",
		name = "io",

		radius = 1811300,
		mass = 8.9319E+22,

		orbit = {
			eccentricity = 0.003545858426216978,
			semi_major_axis = 422018294.5236953,
			arg_of_periapsis = 231.2703460977786,
			mean_anomaly = 195.3274089855250,
			body = "jupiter",
		},

		color = {255, 255, 255},
	},
	{
		type = "planet",
		name = "europa",

		radius = 1550800,
		mass = 4.7998E+22,

		orbit = {
			eccentricity = 0.009511727119926178,
			semi_major_axis = 671253637.5417169,
			arg_of_periapsis = 53.13210737539627,
			mean_anomaly = 276.2652038284650,
			body = "jupiter",
		},

		color = {255, 255, 255},
	},
	{
		type = "planet",
		name = "ganymede",

		radius = 2624100,
		mass = 1.4819E+23,

		orbit = {
			eccentricity = 0.001190086418361844,
			semi_major_axis = 1070823468.894524,
			arg_of_periapsis = 139.2992571342065,
			mean_anomaly = 232.6753228788302,
			body = "jupiter",
		},

		color = {255, 255, 255},
	},
	{
		type = "planet",
		name = "callisto",

		radius = 2409300,
		mass = 1.075938E+23,

		orbit = {
			eccentricity = 0.007973319796896609,
			semi_major_axis = 1883812366.573522,
			arg_of_periapsis = 320.7359683492656,
			mean_anomaly = 15.81614025483249,
			body = "jupiter",
		},

		color = {255, 255, 255},
	},
	{
		type = "planet",
		name = "saturn",

		radius = 57216000,
		mu = 3.7931187e16,

		orbit = {
			eccentricity = 0.05347166506749872,
			semi_major_axis = 1424838758613.269,
			arg_of_periapsis = 85.04661202834268,
			mean_anomaly = 67.46885226487360,
			body = "sun",
		},

		color = {255, 255, 255},
	},
	{
		type = "planet",
		name = "mimas",

		radius = 198200,
		mass = 3.7493E+19,

		orbit = {
			eccentricity = 0.01776275223147744,
			semi_major_axis = 186009285.9220490,
			arg_of_periapsis = 222.2172789396715,
			mean_anomaly = 125.5909781664896,
			body = "saturn",
		},

		color = {255, 255, 255},
	},
	{
		type = "planet",
		name = "enceladus",

		radius = 252100,
		mass = 1.08022E+20,

		orbit = {
			eccentricity = 0.006227897999957464,
			semi_major_axis = 238413699.4838728,
			arg_of_periapsis = 115.5615886062458,
			mean_anomaly = 346.6301476573209,
			body = "saturn",
		},

		color = {255, 255, 255},
	},
	{
		type = "planet",
		name = "tethys",

		radius = 531100,
		mass = 6.17449E+20,

		orbit = {
			eccentricity = 0.001064868868083566,
			semi_major_axis = 294973462.3804425,
			arg_of_periapsis = 215.9196892523803,
			mean_anomaly = 349.8231217220438,
			body = "saturn",
		},

		color = {255, 255, 255},
	},
	{
		type = "planet",
		name = "dione",

		radius = 561400,
		mass = 1.095452E+21,

		orbit = {
			eccentricity = 0.001679230905502774,
			semi_major_axis = 377650651.5017090,
			arg_of_periapsis = 123.6717156049260,
			mean_anomaly = 167.9272784830226,
			body = "saturn",
		},

		color = {255, 255, 255},
	},
	{
		type = "planet",
		name = "rhea",

		radius = 763800,
		mass = 2.306518E+21,

		orbit = {
			eccentricity = 0.001168269515756326,
			semi_major_axis = 527212645.7071990,
			arg_of_periapsis = 172.7367089889645,
			mean_anomaly = 13.48887718956405,
			body = "saturn",
		},

		color = {255, 255, 255},
	},
	{
		type = "planet",
		name = "titan",

		radius = 2573300,
		mass = 1.3452E+23,

		orbit = {
			eccentricity = 0.02891936561555365,
			semi_major_axis = 1221966238.511425,
			arg_of_periapsis = 182.0886765021483,
			mean_anomaly = 75.16117358815676,
			body = "saturn",
		},

		color = {255, 255, 255},
	},
	{
		type = "planet",
		name = "iapetus",

		radius = 734500,
		mass = 1.805E+21,

		orbit = {
			eccentricity = 0.02880286281969610,
			semi_major_axis = 3560162593.022970,
			arg_of_periapsis = 314.3819081366686,
			mean_anomaly = 139.5683324894335,
			body = "saturn",
		},

		color = {255, 255, 255},
	},
	{
		type = "planet",
		name = "uranus",

		radius = 24702000,
		mass = 8.681E+25,

		orbit = {
			eccentricity = 0.04620653158718433,
			semi_major_axis = 2866832853163.975,
			arg_of_periapsis = 169.6876790522249,
			mean_anomaly = 286.8267359944493,
			body = "sun",
		},

		color = {255, 255, 255},
	},
	{
		type = "planet",
		name = "neptune",

		radius = 24085000,
		mass = 1.0243E+26,

		orbit = {
			eccentricity = 0.008090397688364061,
			semi_major_axis = 4497455832811.736,
			arg_of_periapsis = 29.81485402991322,
			mean_anomaly = 162.0995481888285,
			body = "sun",
		},

		color = {255, 255, 255},
	},
	{
		type = "planet",
		name = "pluto",

		radius = 1187000,
		mass = 1.305E+22,

		orbit = {
			eccentricity = 0.2462772488425983,
			semi_major_axis = 5845670624078.223,
			arg_of_periapsis = 184.4945352163909,
			mean_anomaly = 300.1297304812811,
			body = "sun",
		},

		color = {255, 255, 255},
	},
})
