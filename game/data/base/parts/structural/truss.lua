data:extend(
{
	{
		type = "truss",
		name = "truss",

		category = "structural",

		geometry = {
			{-0.5,-0.5},
			{ 0.5,-0.5},
			{ 0.5, 0.5},
			{-0.5, 0.5},
		},

		mount_points = {
			{ 0.0,-0.5},
			{ 0.0, 0.5},
			{-0.5, 0.0},
			{ 0.5, 0.0},
		},
	}
})
