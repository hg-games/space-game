data:extend(
{
	{
		type = "liquid-fuel-engine",
		name = "basic-liquid-fuel-engine",
		
		category = "engine",

		thrust = 60000,

		fuel = {"RP-1", "LOX"},
		fuel_ratio = {9, 11},
		fuel_consumption = 3,

		geometry = {
			{-0.25,-0.5},
			{ 0.25,-0.5},
			{ 0.25, 0.5},
			{-0.25, 0.5},
		},

		mount_points = {
			{0.0,-0.5},
		},
	}
})
