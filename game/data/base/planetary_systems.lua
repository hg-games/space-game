data:extend(
{
	{
		type = "planetary-system",
		name = "solar-system",

		root = "sun",

		bodies = {
			"mercury",

			"venus",

			"earth",
			"moon",

			"mars",
			"deimos",
			"phobos",

			"jupiter",
			"io",
			"europa",
			"ganymede",
			"callisto",

			"saturn",
			"mimas",
			"enceladus",
			"tethys",
			"dione",
			"rhea",
			"titan",
			"iapetus",

			"uranus",

			"neptune",

			"pluto",
		},
	}
})
