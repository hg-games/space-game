# Game directory

Contains a ready to deploy build of the game.
The `data` directory contains the files needed to run the game (textures, sounds, etc).
