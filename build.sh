if [ ! -d "temp" ];
then
	mkdir temp
fi

cd temp
SRC="../source/"

if [ ! $# -eq 1 ];
then
	echo "usage: $0 [build_type]"
	echo "    build type: debug, release, debugoptimized, all"
	echo "    special type: debug_run (build debug and run app)"
	exit 1
fi

build_for() {
	if [ -z "$1" ];
	then
		echo "build_for: must provide a build type"
		exit 1
	fi

	if [[ ! -d "build_$1" ]] || [[ ! -f "build_$1/build.ninja" ]];
	then
		meson --buildtype "$1" "$SRC" "build_$1"
	fi
	[[ $? -eq 0 ]] && (cd "build_$1" && ninja) && return 0
}

case "$1" in
	debug) ;&
	release) ;&
	debugoptimized)
		build_for "$1" ;;
	all)
		build_for debug &&
		build_for release &&
		build_for debugoptimized ;;
	debug_run)
		build_for debugoptimized &&
		cd ../game/ && "../temp/build_debugoptimized/game01"
		;;
	*) echo "unknown build type $1" ;;
esac
